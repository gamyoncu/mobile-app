export const GENDER_OPTIONS = [
  {
    title: 'xNotSpecified',
    value: 0,
  },
  {
    title: 'xMan',
    value: 1,
  },
  {
    title: 'xWoman',
    value: 2,
  },
];

export const USER_TYPE_OPTIONS = [
  {
    title: 'xUser',
    value: 0,
  },
  {
    title: 'xCoach',
    value: 1,
  },
];

export const LANGUAGE_OPTIONS = [
  {
    value: 0,
    title: 'xSwedishSE',
  },
  {
    value: 1,
    title: 'xEnglishUS',
  },
  {
    value: 2,
    title: 'xTurkishTurkey',
  },
];
