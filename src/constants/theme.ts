import {
  DefaultTheme as RNDefaultTheme,
  DarkTheme as RNDarkTheme,
  Theme,
} from '@react-navigation/native';

declare global {
  interface AppTheme extends Theme {
    dark: boolean;
    colors: {
      background: string;
      card: string;
      text: string;
      border: string;
      notification: string;

      // tint colors
      primary: string;
      'primary-pressed': string;
      'on-primary': string;

      'primary-variant': string;
      'primary-variant-pressed': string;
      'on-primary-variant': string;

      secondary: string;
      'secondary-pressed': string;
      'on-secondary': string;

      success: string;
      'success-pressed': string;
      'on-success': string;

      warning: string;
      'warning-pressed': string;
      'on-warning': string;

      danger: string;
      'danger-pressed': string;
      'on-danger': string;

      // background colors
      bg: string;
      overlay: string;
      'input-bg': string;
      'card-bg': string;
      'surface-primary': string;
      'surface-secondary': string;
      'surface-tertiary': string;
      'surface-success': string;
      'surface-warning': string;
      'surface-danger': string;

      // foreground colors
      'high-emphasis': string;
      'medium-emphasis': string;
      'low-emphasis': string;
      'reversed-emphasis': string;
      placeholder: string;

      'main-bg': string;
    };
  }
}

export const DefaultTheme = {
  ...RNDefaultTheme,
  colors: {
    ...RNDefaultTheme.colors,
    primary: '#2e5fe3',
    'primary-pressed': '#979DE5',
    'on-primary': '#FFFFFF',

    'primary-variant': '#E7F1FC',
    'primary-variant-pressed': '#EFF7FF',
    'on-primary-variant': '#6971DA',

    secondary: '#77E5F6',
    'secondary-pressed': '#B1ECF5',
    'on-secondary': '#223754',

    success: '#63D09F',
    'success-pressed': '#9BEAC6',
    'on-success': '#FFFFFF',

    warning: '#FACE33',
    'warning-pressed': '#FFDD67',
    'on-warning': '#223754',

    danger: '#E55752',
    'danger-pressed': '#FF908C',
    'on-danger': '#FFFFFF',

    bg: '#f4f4f4',
    overlay: 'rgba(0,0,0,0.75)',
    'input-bg': '#F4F5FC',
    'card-bg': '#FFFFFF',
    'surface-primary': '#F9FAFD',
    'surface-secondary': '#D9FAFF',
    'surface-tertiary': '#BFCFE8',
    'surface-success': '#EAF9F4',
    'surface-warning': '#FFF8DF',
    'surface-danger': '#FFDEDD',

    // foreground colors
    'high-emphasis': '#223754',
    'medium-emphasis': '#7A8798',
    'low-emphasis': 'rgba(34,55,84,0.3)',
    'reversed-emphasis': '#FFFFFF',
    placeholder: '#C6D0DE',

    'main-bg': 'F3F4F8',
  },
};

export const DarkTheme = {
  ...RNDarkTheme,
  colors: {
    ...RNDarkTheme.colors,
    'primary-pressed': '',
    'on-primary': '',

    'primary-variant': '',
    'primary-variant-pressed': '',
    'on-primary-variant': '',

    secondary: '',
    'secondary-pressed': '',
    'on-secondary': '',

    success: '',
    'success-pressed': '',
    'on-success': '',

    warning: '',
    'warning-pressed': '',
    'on-warning': '',

    danger: '',
    'danger-pressed': '',
    'on-danger': '',
  },
};
