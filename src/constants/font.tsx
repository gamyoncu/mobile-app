declare global {
  interface FontStyle {
    fontFamily: string;
    fontSize: number;
    lineHeight: number;
  }

  interface FontStyles {
    h1: undefined;
    h2: undefined;
    h3: undefined;
    h4: undefined;
    title: undefined;
    subtitle: undefined;
    button: undefined;
    link: undefined;
    body1: undefined;
    body2: undefined;
    helper: undefined;
    caption: undefined;
    overline: undefined;
    label: undefined;
  }
}

export const FONT_STYLES = {
  h1: {
    fontFamily: 'monserrat-bold',
    fontSize: 60,
    lineHeight: 48,
  },
  h2: {
    fontFamily: 'monserrat-bold',
    fontSize: 34,
    lineHeight: 48,
  },
  h3: {
    fontFamily: 'monserrat-semibold',
    fontSize: 24,
    lineHeight: 32,
  },
  h4: {
    fontFamily: 'monserrat-semibold',
    fontSize: 20,
    lineHeight: 28,
  },
  title: {
    fontFamily: 'monserrat-semibold',
    fontSize: 18,
    lineHeight: 24,
  },
  subtitle: {
    fontFamily: 'monserrat-medium',
    fontSize: 14,
    lineHeight: 24,
  },
  button: {
    fontFamily: 'monserrat-semibold',
    fontSize: 16,
    lineHeight: 24,
  },
  link: {
    fontFamily: 'monserrat-semibold',
    fontSize: 12,
    lineHeight: 20,
  },
  body1: {
    fontFamily: 'monserrat-regular',
    fontSize: 18,
    lineHeight: 24,
  },
  body2: {
    fontFamily: 'monserrat-regular',
    fontSize: 16,
    lineHeight: 24,
  },
  helper: {
    fontFamily: 'monserrat-semibold',
    fontSize: 14,
    lineHeight: 20,
  },
  caption: {
    fontFamily: 'monserrat-regular',
    fontSize: 12,
    lineHeight: 20,
  },
  overline: {
    fontFamily: 'monserrat-bold',
    fontSize: 12,
    lineHeight: 16,
  },
  label: {
    fontFamily: 'monserrat-medium',
    fontSize: 12,
    lineHeight: 16,
  },
};
