import { useTheme } from '@react-navigation/native';
import React from 'react';
import { Block } from '../../components';

export function Avatar({ size = 36, ...rest }) {
  // theme
  const { colors } = useTheme() as AppTheme;

  return (
    <Block
      width={size}
      height={size}
      borderRadius={size / 2}
      color={colors.warning}
      {...rest}
    />
  );
}
