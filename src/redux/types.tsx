// redux constants

// base
export const RESET_REDUCER = (stateKey: string) => `${stateKey}_RESET_REDUCER`;
export const CHANGE_REDUCER = (stateKey: string) =>
  `${stateKey}_CHANGE_REDUCER`;

// reducers
export const REDUCERS = {
  APP: 'app',
  STATUS: 'status',
  AUTH: 'auth',
  USER: 'user',
  REGIONAL: 'regional',

  IMAGE: 'image',
  ADDRESS: 'address',
};
