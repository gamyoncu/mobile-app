import { combineReducers } from 'redux';
import { REDUCERS } from '../types';
import { initReducer } from './base';

// reducers
interface AppReducerProps {
  firstOpen: boolean;
  theme: 'light' | 'dark';
  openSendAdvert: boolean;

  // from api
  forceUpdate: boolean;
  iosUrl: string;
  androidUrl: string;
}
const app = initReducer(REDUCERS.APP, {
  firstOpen: true,
  theme: 'light',
  openSendAdvert: false,

  forceUpdate: false,
  iosUrl: '',
  androidUrl: '',
});

const status = initReducer(REDUCERS.STATUS, {});

interface AuthReducerProps {
  token: string;
  userTypeId: number;
}
const auth = initReducer(REDUCERS.AUTH, {
  token: '',
  userTypeId: 0,
});
// end reducers

declare global {
  interface AppRootState {
    app: AppReducerProps;
    status: any;
    auth: AuthReducerProps;
  }
}

// combine all reducers
const rootReducer = combineReducers<AppRootState>({
  app,
  status,
  auth,
});

export default rootReducer;
