import { RESET_REDUCER, CHANGE_REDUCER } from '../types';
// api imports
import { SERVER_URL } from '../../constants/config';
import { store } from '../../redux/store';
import { getLocale } from '../../localization';
import { showFlashMessage } from '../../components/flashMessage/message';

class ActionBase {
  constructor(stateKey: string, path: string) {
    this.stateKey = stateKey;
    this.path = path;
  }
  // redux
  stateKey = '';
  path = '';
  resetReducer = (props?: object | null) => (dispatch: Function) => {
    dispatch({
      type: RESET_REDUCER(this.stateKey),
      ...props,
    });
  };

  changeReducer = (props?: object | null) => (dispatch: Function) => {
    dispatch({
      type: CHANGE_REDUCER(this.stateKey),
      ...props,
    });
  };

  // API call
  getUrl(endpoint: string) {
    return SERVER_URL + this.path + endpoint;
  }

  async getHeaders(isLoginRequired: boolean) {
    var tokenHeader = {};
    if (isLoginRequired) {
      tokenHeader = { authorization: 'Bearer ' + store.getState().auth.token };
    }
    return {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'accept-language': await getLocale(),
      ...tokenHeader,
    };
  }

  async post(endpoint: string, data: object, isLoginRequired: boolean = false) {
    const url = this.getUrl(endpoint);
    console.log('url', url);

    try {
      const res = await fetch(url, {
        method: 'POST',
        headers: await this.getHeaders(isLoginRequired),
        body: JSON.stringify(data),
      });
      const json = await res.json();

      console.log('json', endpoint, json, res.status);

      if (
        json &&
        json.Message &&
        json.Message ===
          'No session found due to given token or token has exprired.'
      ) {
      }

      this.showAlert(json);

      return json;
    } catch (error) {
      console.error('ERROR :: POST REQUEST :: URL :: ', url);
      console.error('ERROR :: POST REQUEST :: ', error);
    }

    return null;
  }

  async get(endpoint: string, isLoginRequired: boolean = false) {
    const url = this.getUrl(endpoint);
    console.log('url', url);

    try {
      const res = await fetch(url, {
        method: 'GET',
        headers: await this.getHeaders(isLoginRequired),
      });
      const json = await res.json();
      this.showAlert(json);
      return json;
    } catch (error) {
      console.error('ERROR :: POST REQUEST :: URL :: ', url);
      console.error('ERROR :: POST REQUEST :: ', error);
    }
    return null;
  }

  showAlert(json: any) {
    if (json.Message) {
      // TODO localization
      if (json.IsSuccess) showFlashMessage('success', json.Message);
      else showFlashMessage('danger', json.Message);
    }
  }
}

export { ActionBase };
