import { REDUCERS } from '../types';
import { ActionBase } from './base';
import { status } from './status';

declare global {
  interface LoginReq {
    email: string;
    password: string;
  }

  interface RegisterReq {
    userTypeId: number;
    email: string;
    password: string;
    fullname: string;
  }
}

class AuthModel extends ActionBase {
  handleAuth(res: any) {
    return (dispatch: any) => {
      if (res.Dto && res.Dto.Token) {
        dispatch(this.changeReducer({ ...res.Dto }));
      }
    };
  }

  login(data: LoginReq) {
    return async (dispatch: any) => {
      dispatch(status.changeReducer({ login: true }));
      const res = await this.post(`/login`, data);
      if (res) await dispatch(this.handleAuth(res));
      dispatch(status.changeReducer({ login: false }));
    };
  }

  register(data: RegisterReq) {
    return async (dispatch: any) => {
      dispatch(status.changeReducer({ register: true }));
      const res = await this.post(`/register`, data);
      if (res) await dispatch(this.handleAuth(res));
      dispatch(status.changeReducer({ register: false }));
    };
  }

  logout() {
    return (dispatch: any) => {
      dispatch(this.resetReducer());
    };
  }
}

const auth = new AuthModel(REDUCERS.AUTH, '/auth');

export { auth };
