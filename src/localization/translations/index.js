export const languages = {
  en: { en: 'English', tr: 'Turkish', Login: 'Login', 'Sign up': 'Sign up' },
  tr: {
    en: 'English',
    tr: 'Turkish',
    Login: 'Giriş Yap',
    'Sign up': 'Kayıt Ol',
  },
};
