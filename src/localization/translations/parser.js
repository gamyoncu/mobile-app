const csv = require('csvtojson')
const fs = require('fs');

async function main() {
    const csvFilePath = './localization.csv'

    const jsonArray = await csv().fromFile(csvFilePath);

    console.log(jsonArray)

    var languages = {};

    jsonArray.forEach(row => {

        Object.keys(row).forEach(colKey => {
            if (colKey !== "Key" && colKey !== "Comment") {
                if (languages.hasOwnProperty(colKey)) {
                    languages = {
                        ...languages,
                        [colKey]: {
                            ...languages[colKey],
                            [row["Key"]]: row[colKey]
                        }
                    }
                } else {
                    languages = {
                        ...languages,
                        [colKey]: {
                            [row["Key"]]: row[colKey]
                        }
                    }
                }
            }
        })
    })

    console.log(languages)

    const jsFile = `export const languages = ${JSON.stringify(languages)}`
    fs.writeFile("./index.js", jsFile, "utf-8", (error) => {
        if (error) console.log('error', error)
        else console.log('Localization files generated')
    })
}

main()