import * as Localization from 'expo-localization';
import i18n from 'i18n-js';
import * as db from '../db';
import { languages } from './translations';

i18n.translations = languages;

export async function startLocalization() {
  // get language
  const language = await db.localization.get();
  if (language) {
    i18n.locale = language;
  } else {
    const lang = Localization.locale;
    if (Object.keys(languages).includes(lang)) i18n.locale = lang;
    else i18n.locale = 'en';
  }
  // TODO later
  // moment.locale(i18n.locale)
  // LocaleConfig.Defaults ocale = i18n.locale;
  await db.localization.set(i18n.locale);
}

export async function getLocalizeCurrentLanguage() {
  const lang = await getLocale();
  return i18n.t(`languages.${lang}`);
}

export function getLanguageOptions() {
  const translations = Object.keys(i18n.translations);
  const options = translations.map((translation) => {
    return {
      title: i18n.t(`${translation}`),
      value: translation,
    };
  });
  return options;
}

export async function changeLanguage(language) {
  await db.localization.set(language);
  await startLocalization();
}

export function getLocale() {
  // await startLocalization()
  const lang = i18n.locale;
  if (Object.keys(languages).includes(lang)) return lang;
  else return 'en';
}
