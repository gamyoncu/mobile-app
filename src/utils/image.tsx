import * as ImagePicker from 'expo-image-picker';

export const requestMediaLibraryPermissionsAsync = async () => {
  const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
  if (status !== 'granted') {
    alert('Sorry, we need camera roll permissions to make this work!');
  }
};

export const launchImageLibraryAsync = async (
  aspect: [number, number] = [4, 4]
) => {
  return await ImagePicker.launchImageLibraryAsync({
    mediaTypes: ImagePicker.MediaTypeOptions.All,
    allowsEditing: true,
    aspect,
    quality: 0.7,
    base64: true,
  });
};

export const requestCameraPermissionsAsync = async () => {
  const { status } = await ImagePicker.requestCameraPermissionsAsync();
  if (status !== 'granted') {
    alert('Sorry, we need camera roll permissions to make this work!');
  }
};

export const launchCameraAsync = async (aspect: [number, number] = [4, 4]) => {
  return await ImagePicker.launchCameraAsync({
    mediaTypes: ImagePicker.MediaTypeOptions.All,
    allowsEditing: true,
    aspect,
    quality: 0.7,
    base64: true,
  });
};
