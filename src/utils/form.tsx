const isValidEmail = (email: string) => {
  const emailRegex =
    /^[-!#$%&'*+\/0-9=?A-Z^_a-z{|}~](\.?[-!#$%&'*+\/0-9=?A-Z^_a-z`{|}~])*@[a-zA-Z0-9](-*\.?[a-zA-Z0-9])*\.[a-zA-Z](-?[a-zA-Z0-9])+$/;

  if (!email) return false;

  if (email.length > 254) return false;

  const valid = emailRegex.test(email);
  if (!valid) return false;

  // Further checking of some things regex can't handle
  const parts = email.split('@');
  if (parts[0].length > 64) return false;

  const domainParts = parts[1].split('.');
  if (
    domainParts.some(function (part) {
      return part.length > 63;
    })
  )
    return false;

  return true;
};

const isValidPassword = (password: string) => {
  if (password.length >= 4) return true;
  return false;
};

const isValidFullname = (fullname: string) => {
  if (fullname.length >= 3) return true;
  return false;
};

export const checkErrorOnForm = (data: any) => {
  var warning = {
    fullname: '',
    email: '',
    password: '',
  };
  Object.keys(data).forEach((key) => {
    if (key === 'email' && !isValidEmail(data['email']))
      warning = {
        ...warning,
        email: 'You should write valid email',
      };
    if (key === 'fullname' && !isValidFullname(data['fullname']))
      warning = {
        ...warning,
        fullname: 'You should write valid fullname',
      };
    if (key === 'password' && !isValidPassword(data['password']))
      warning = {
        ...warning,
        password: 'You should write valid password',
      };
  });

  return warning;
};
