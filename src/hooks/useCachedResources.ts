import { FontAwesome } from '@expo/vector-icons';
import * as Font from 'expo-font';
import * as SplashScreen from 'expo-splash-screen';
import * as React from 'react';

export default function useCachedResources() {
  const [isLoadingComplete, setLoadingComplete] = React.useState(false);

  // Load any resources or data that we need prior to rendering the app
  React.useEffect(() => {
    async function loadResourcesAndDataAsync() {
      try {
        SplashScreen.preventAutoHideAsync();

        // Load fonts
        await Font.loadAsync({
          ...FontAwesome.font,
          'monserrat-regular': require('../assets/fonts/Montserrat/Montserrat-Regular.ttf'),
          'monserrat-medium': require('../assets/fonts/Montserrat/Montserrat-Medium.ttf'),
          'monserrat-semibold': require('../assets/fonts/Montserrat/Montserrat-SemiBold.ttf'),
          'monserrat-bold': require('../assets/fonts/Montserrat/Montserrat-Bold.ttf'),
        });
      } catch (e) {
        // We might want to provide this error information to an error reporting service
        console.warn(e);
      } finally {
        setLoadingComplete(true);
        SplashScreen.hideAsync();
      }
    }

    loadResourcesAndDataAsync();
  }, []);

  return isLoadingComplete;
}
