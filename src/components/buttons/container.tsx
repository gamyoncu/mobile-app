import { FontAwesome, MaterialIcons } from '@expo/vector-icons';
import { useTheme } from '@react-navigation/native';
import React from 'react';
import {
  TouchableOpacity,
  TouchableOpacityProps,
  Text,
  Block,
} from '../common';

export interface ButtonContainerProps extends TouchableOpacityProps {
  height?: number | string;
  margin?: Array<number> | number;

  iconName?: React.ComponentProps<typeof FontAwesome>['name'];
  iconColor?: string;
  title?: string;
  titleColor?: string;

  loading?: boolean; // TODO code
}

export function ButtonContainer({
  height = 48,
  margin = [8],
  iconName,
  iconColor,
  title,
  titleColor,
  ...rest
}: ButtonContainerProps) {
  const { colors } = useTheme() as AppTheme;

  return (
    <TouchableOpacity
      height={height}
      borderRadius
      margin={margin}
      padding={[0, 16]}
      alignCenter
      row
      color={colors.primary}
      style={{
        borderRadius: 24,
      }}
      {...rest}
    >
      {iconName ? (
        <Block margin={[0, 0, 0, 4]}>
          <FontAwesome
            size={20}
            name={iconName}
            color={colors['surface-primary']}
            margin={8}
          />
        </Block>
      ) : null}
      <Block flex alignCenter>
        <Text fontFamily={'button'} color={colors['on-primary']}>
          {title}{' '}
        </Text>
      </Block>
    </TouchableOpacity>
  );
}
