import { useTheme } from '@react-navigation/native';
import React from 'react';
import { TouchableOpacity, TouchableOpacityProps, Text } from '../common';

export interface ButtonTextProps extends TouchableOpacityProps {
  title?: string;
  titleColor?: string;
}

export function ButtonText({ title, titleColor, ...rest }: ButtonTextProps) {
  const { colors } = useTheme() as AppTheme;

  return (
    <TouchableOpacity
      borderRadius
      padding={[0, 16]}
      alignCenter
      row
      style={{
        borderRadius: 24,
      }}
      {...rest}
    >
      <Text
        fontFamily={'helper'}
        color={titleColor || colors.primary}
        style={{ textAlign: 'center' }}
      >
        {title}
      </Text>
    </TouchableOpacity>
  );
}
