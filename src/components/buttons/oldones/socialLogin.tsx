import { FontAwesome, MaterialIcons } from '@expo/vector-icons';
import React from 'react';
import {
  TouchableOpacity,
  TouchableOpacityProps,
  Text,
  Block,
} from '../../common';

export interface ButtonSocialLoginProps extends TouchableOpacityProps {
  iconName?: React.ComponentProps<typeof FontAwesome>['name'];
  iconColor?: string;
  title?: string;
  titleColor?: string;
}

export function ButtonSocialLogin({
  iconName,
  iconColor,
  title,
  titleColor,
  ...rest
}: ButtonSocialLoginProps) {
  return (
    <TouchableOpacity
      width={240}
      borderRadius
      margin={[8, 0]}
      padding={[12, 8]}
      alignCenter
      row
      {...rest}
    >
      <Block margin={[0, 8]}>
        <FontAwesome size={20} name={iconName} color={iconColor} margin={8} />
      </Block>
      <Text color={titleColor}>{title} </Text>
    </TouchableOpacity>
  );
}
