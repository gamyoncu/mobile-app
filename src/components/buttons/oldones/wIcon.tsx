import React, { ComponentType } from 'react';
import { TouchableOpacity, TouchableOpacityProps, Text } from '../../common';
import { useTheme } from '@react-navigation/native';
import { ActivityIndicator } from 'react-native';

export interface ButtonWIconProps extends TouchableOpacityProps {
  title: string;
  loading?: boolean;

  left?: ComponentType | undefined;
  right?: ComponentType | undefined;
}

export function ButtonWIcon({
  title,
  loading,
  left,
  right,
  ...rest
}: ButtonWIconProps) {
  // theme
  const { colors } = useTheme() as AppTheme;

  return (
    <TouchableOpacity
      padding={[12, 16]}
      margin={8}
      justifyCenter
      alignCenter
      borderRadius
      color={colors.secondary}
      width={'90%'}
      disabled={loading}
      {...rest}
    >
      {left}
      {loading ? (
        <ActivityIndicator color={colors.white} />
      ) : (
        <Text bold color={colors.card}>
          {title}
        </Text>
      )}
      {right}
    </TouchableOpacity>
  );
}
