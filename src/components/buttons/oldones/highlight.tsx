import React from 'react';
import { TextStyle } from 'react-native';
import { FindAllArgs } from 'highlight-words-core';
import { TouchableOpacity, TouchableOpacityProps } from '../../common';
import { HighlightText } from '../../highlightText'
import { useTheme } from '@react-navigation/native';

export interface ButtonHighlightProps extends TouchableOpacityProps, FindAllArgs {
    highlightStyle?: TextStyle;
    textStyle?: TextStyle;
}

export function ButtonHighlight({
    textStyle,
    // HighlightText
    highlightStyle,
    searchWords,
    textToHighlight,
    ...rest
}: ButtonHighlightProps) {

    // theme
    const { colors } = useTheme() as AppTheme;

    return (<TouchableOpacity
        padding={[12, 16]}
        margin={8}
        justifyCenter
        alignCenter
        {...rest}
    >
        <HighlightText
            highlightStyle={highlightStyle || {
                color: colors.secondary,
                fontWeight: "800",
            }}
            style={textStyle}
            searchWords={searchWords}
            textToHighlight={textToHighlight}
        />
    </TouchableOpacity>)
}