import React from 'react';
import { Block, Text } from './common';

interface BadgeBooleanProps {
  active: boolean;

  activeTitle?: string;
  inActiveTitle?: string;

  activeColor?: string;
  inActiveColor?: string;
}

export function BadgeBoolean({
  active,
  activeTitle = 'Active',
  inActiveTitle = 'Inactive',
  activeColor = 'green',
  inActiveColor = 'red',
}: BadgeBooleanProps) {
  return (
    <Block
      color={active ? activeColor : inActiveColor}
      justifyCenter
      alignCenter
      borderRadius
      margin={[4]}
      padding={[4]}
    >
      <Text color={'white'}> {active ? activeTitle : inActiveTitle} </Text>
    </Block>
  );
}
