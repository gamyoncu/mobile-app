import { showMessage as showMessageBase } from "./lib";

/**
 *
 *
 * @export
 * @param {("success" | "none" | "default" | "info" | "danger" | "warning")} type
 * @param {string} [title=""]
 * @param {string | undefined} message
 */
export function showFlashMessage(
  type,
  title,
  message = ""
) {
  showMessageBase({
    type,
    message: title,
    description: message,
    animationDuration: 500
  });
}
