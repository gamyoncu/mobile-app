import { MaterialIcons } from '@expo/vector-icons';
import { useTheme } from '@react-navigation/native';
import React, { Fragment, useState } from 'react';
import { TouchableOpacity, TouchableOpacityProps, Text } from '../common';
import { AddPhotoModal } from './addPhotoModal';

export interface ButtonAddAvatarPhotoProps extends TouchableOpacityProps {
  size?: number | undefined;
}

export function ButtonAddAvatarPhoto({
  size = 90,
  ...rest
}: ButtonAddAvatarPhotoProps) {
  // theme
  const { colors } = useTheme() as AppTheme;

  // state
  const [show, setShow] = useState(false);

  // helper functions
  const handleAddPhoto = (result: any) => {
    // TODO
    console.log('result', result);
    setShow(false);
  };

  return (
    <Fragment>
      <TouchableOpacity
        width={size}
        height={size}
        borderRadius={size / 2}
        color={colors.card}
        borderWidth
        justifyCenter
        alignCenter
        onPress={() => setShow(true)}
        {...rest}
      >
        <MaterialIcons
          name={'person'}
          size={size / 2.3}
          color={colors.secondary}
        />
        <Text
          h7
          style={{ textAlign: 'center', fontFamily: 'monserrat-bold' }}
          color={colors.secondary}
        >
          ADD{'\n'}PICTURE
        </Text>
      </TouchableOpacity>
      <AddPhotoModal open={show} onPressClose={handleAddPhoto} />
    </Fragment>
  );
}
