import { useTheme } from '@react-navigation/native';
import React from 'react';
import { imageHandler } from '../../utils';
import { ButtonText } from '../buttons';
import { Modal, ModalProps, Block } from '../common';
import { showFlashMessage } from '../flashMessage/message';

export interface AddPhotoModalProps extends ModalProps {
  aspect?: [number, number];

  onSelectPhoto: (result: any) => void;
}

export function AddPhotoModal({
  open,
  onPressClose,
  onSelectPhoto,
  aspect = [4, 4],
}: AddPhotoModalProps) {
  // theme
  const { colors } = useTheme() as AppTheme;

  const pickImageWithCamera = async () => {
    try {
      var result = await imageHandler.launchCameraAsync(aspect);
      onSelectPhoto(result);
    } catch (error) {
      // console.error("ERROR :: pick Image", error);
      showFlashMessage('warning', 'Error happen while picking image'); // TODO localization
    }
  };

  const pickImageWithGallery = async () => {
    try {
      var result = await imageHandler.launchImageLibraryAsync(aspect);
      onSelectPhoto(result);
    } catch (error) {
      // console.error("ERROR :: pick Image", error);
      showFlashMessage('warning', 'Error happen while picking image'); // TODO localization
    }
  };

  return (
    <Modal open={open} onPressClose={onPressClose}>
      <Block flex color={colors.card} borderRadius alignCenter justifyCenter>
        <ButtonText
          title={'Take a photo'}
          onPress={() => pickImageWithCamera()}
          margin={[8]}
        />
        <ButtonText
          title={'Choose from Library'}
          onPress={() => pickImageWithGallery()}
          margin={[8]}
        />
      </Block>
    </Modal>
  );
}
