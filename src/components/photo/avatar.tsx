import { useTheme } from '@react-navigation/native';
import React from 'react';
import { useSelector } from 'react-redux';
import { Image } from '../common';

// TODO change avatar
export interface AvatarProps {
  size?: number | undefined;
}

export function Avatar({ size = 40, ...rest }: AvatarProps) {
  // theme
  const { colors } = useTheme() as AppTheme;

  // redux
  const {
    auth: { profileImageName },
  } = useSelector((state) => state) as AppRootState;

  return (
    <Image
      source={{
        uri: 'https://www.oseyo.co.uk/wp-content/uploads/2020/05/empty-profile-picture-png-2.png',
      }}
      width={size}
      height={size}
      style={{
        width: size,
        height: size,
        borderRadius: size / 2,
        backgroundColor: colors.backgroundModal,
      }}
      {...rest}
    />
  );
}
