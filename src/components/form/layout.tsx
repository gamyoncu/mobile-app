import { FontAwesome } from '@expo/vector-icons';
import { useTheme } from '@react-navigation/native';
import React from 'react';
import {
  TouchableOpacity,
  TouchableOpacityProps,
  Text,
  TextProps,
  Block,
} from '../common';
import { TextInput, TextInputProps } from 'react-native';
import { FormTitle, FormTitleProps } from './title';
import { FONT_STYLES } from '../../constants/font';

export interface FormLayoutProps extends TouchableOpacityProps {
  title?: string;
  titleProps?: FormTitleProps;

  warning?: string;
  warningProps?: TextProps;

  value?: string;
  placeholder?: string;
  CenterComponent?: typeof Text | typeof TextInput | any;
  centerComponentProps?: any;

  left?: React.ReactElement;

  leftIconName?: React.ComponentProps<typeof FontAwesome>['name'] | null;

  rightIconName?: React.ComponentProps<typeof FontAwesome>['name'] | null;
  rightIconProps?: TouchableOpacityProps;
  rightIconColor?: string | null;
  rightIconTouchable?: boolean;
}

export function FormLayout({
  disabled = true,

  title,
  titleProps,

  warning,
  warningProps,

  value,
  placeholder,
  CenterComponent = Text,
  centerComponentProps,

  left,
  leftIconName,

  rightIconName,
  rightIconProps,
  rightIconColor,
  rightIconTouchable = false,

  ...rest
}: FormLayoutProps) {
  // theme
  const { colors } = useTheme() as AppTheme;

  return (
    <TouchableOpacity disabled={disabled} margin={[4, 0]} {...rest}>
      {title ? (
        <FormTitle fontFamily={'caption'} {...titleProps}>
          {title}
        </FormTitle>
      ) : null}
      <Block
        row
        height={48}
        color={colors['input-bg']}
        borderRadius={12}
        alignCenter
        width={'100%'}
      >
        <Block row between flex alignCenter>
          {left ? (
            <Block width={32} margin={[0, 16, 0, 0]} alignCenter justifyCenter>
              {left}
            </Block>
          ) : null}
          {leftIconName ? (
            <Block width={32} margin={[0, 16, 0, 0]} alignCenter justifyCenter>
              <FontAwesome
                name={leftIconName}
                size={24}
                color={value ? colors['high-emphasis'] : colors['low-emphasis']}
              />
            </Block>
          ) : null}
          <CenterComponent
            placeholderTextColor={colors['low-emphasis']}
            color={value ? colors['high-emphasis'] : colors['low-emphasis']}
            style={[
              {
                width: rightIconName ? '90%' : '100%',
                fontSize: 14,
                color: value ? colors['high-emphasis'] : colors['low-emphasis'],
                paddingHorizontal: 8,
                paddingVertical: 8,
              },
              // style,
              {
                borderRadius: 12,
              },
              {
                ...FONT_STYLES.helper,
              },
            ]}
            {...centerComponentProps}
          >
            {value || placeholder}
          </CenterComponent>
          {rightIconName && (
            <TouchableOpacity
              disabled={!rightIconTouchable}
              padding={[0, 4]}
              alignCenter
              justifyCenter
              {...rightIconProps}
            >
              <FontAwesome
                name={rightIconName}
                size={24}
                color={rightIconColor || colors['low-emphasis']}
              />
            </TouchableOpacity>
          )}
        </Block>
      </Block>
      <Text
        fontFamily={'caption'}
        color={colors.danger}
        margin={[2, 0, 0, 0]}
        {...warningProps}
      >
        {warning}
      </Text>
    </TouchableOpacity>
  );
}
