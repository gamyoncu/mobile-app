import React, { Fragment, useState, useEffect } from 'react';
import { PickerModal } from './modal';
import { FormLayout, FormLayoutProps } from '../layout';
import { useTheme } from '@react-navigation/native';

export interface InputPickerProps extends FormLayoutProps {
  options: Array<Option>;
  value?: number;
  defaultValue?: Option;

  onChangeValue?: (option: Option) => void;
}

export function InputPicker({
  title,
  options,
  value,
  onChangeValue,
  defaultValue = {
    title: '',
    value: 0,
  },
  ...rest
}: InputPickerProps) {


  // state
  const [show, setShow] = useState(false);
  const [selectedOption, setSelectedOption] = useState(defaultValue);

  // useEffect(() => {
  //   options.forEach((option) => {
  //     if (option.value === value) setSelectedOption(option);
  //   });
  // }, [value]);

  useEffect(() => {
    if (onChangeValue) onChangeValue(selectedOption);
  }, [selectedOption]);

  return (
    <Fragment>
      <FormLayout
        title={title}
        value={
          selectedOption && selectedOption.title ? selectedOption.title : ''
        }
        disabled={false}
        onPress={() => setShow(true)}
        {...rest}
      />
      {show && (
        <PickerModal
          open={show}
          onPressClose={(result: any) => {
            if (result) {
              setSelectedOption(result);
            }
            setShow(false);
          }}
          title={title}
          options={options}
        />
      )}
    </Fragment>
  );
}
