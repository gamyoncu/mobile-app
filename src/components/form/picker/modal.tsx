import React, { useState } from 'react';
import {
  Modal,
  ModalProps,
  Block,
  Text,
  TouchableOpacity,
  TouchableOpacityProps,
  ScrollBlock,
} from '../../common';
import { ButtonWIcon } from '../../buttons';
import { useTheme } from '@react-navigation/native';

export interface PickerModalProps extends ModalProps {
  title?: string;
  defaultValue?: any;

  options?: Array<Option>;
}

export function PickerModal({
  // modal base props
  open,
  onPressClose,

  // date spesifix props
  title = '',
  defaultValue,

  options,
}: PickerModalProps) {
  // theme
  const { colors } = useTheme() as AppTheme;

  // const [title, setTitle] = useState(value ? moment(value).format('l') : '');
  const [value, setValue] = useState(defaultValue);

  return (
    <Modal
      open={open}
      onPressClose={onPressClose}
      height={400}
      // maxHeight={300}
    >
      <Block flex color={colors.bg} borderRadius justifyCenter>
        <Block flex alignStart padding={[12]}>
          <ScrollBlock style={{ backgroundColor: colors.bg }}>
            {options?.map((option, index) => (
              <OptionItem
                key={index.toString()}
                option={option}
                onPress={() => {
                  if (onPressClose) onPressClose(option);
                }}
              />
            ))}
          </ScrollBlock>
        </Block>
      </Block>
    </Modal>
  );
}

export interface OptionItemProps extends TouchableOpacityProps {
  option: Option;
}

function OptionItem({ option, ...rest }: OptionItemProps) {
  return (
    <TouchableOpacity margin={[8, 16]} {...rest}>
      <Text h5>{option.title}</Text>
    </TouchableOpacity>
  );
}
