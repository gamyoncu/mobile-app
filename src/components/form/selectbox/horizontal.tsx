import { useTheme } from '@react-navigation/native';
import React, { useState } from 'react';
import {
  Block,
  Text,
  TouchableOpacity,
  TouchableOpacityProps,
} from '../../common';

declare global {
  type Option = { value: any; title: string };
}
interface SelectboxHorizontalItemProps extends TouchableOpacityProps {
  option: Option;

  selected?: boolean | undefined;
}
function SelectboxHorizontalItem({
  selected,
  option: { title, value },
  ...rest
}: SelectboxHorizontalItemProps) {
  const { colors } = useTheme() as AppTheme;

  const b_size = 20;
  const s_size = 12;

  return (
    <TouchableOpacity row alignCenter margin={4} {...rest}>
      <Block
        width={b_size}
        height={b_size}
        borderRadius={b_size / 2}
        color={colors.white}
        alignCenter
        justifyCenter
      >
        <Block
          width={s_size}
          height={s_size}
          borderRadius={s_size / 2}
          color={selected ? colors.secondary : colors.white}
        />
      </Block>
      <Text margin={[0, 6]} color={colors.white}>
        {title}
      </Text>
    </TouchableOpacity>
  );
}

export interface SelectboxHorizontalProps {
  options: Array<Option>;
}
export function SelectboxHorizontal({ options }: SelectboxHorizontalProps) {
  // theme
  const { colors } = useTheme() as AppTheme;

  // state
  const [selected, setSelected] = useState<Option | null>(null);

  return (
    <Block margin={[8]} row around width={'100%'} color={colors.white}>
      {options.map((option, index) => (
        <SelectboxHorizontalItem
          key={index.toString()}
          option={option}
          selected={selected ? option.value === selected.value : false}
          onPress={() => setSelected(option)}
        />
      ))}
    </Block>
  );
}
