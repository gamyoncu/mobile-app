import { useTheme } from '@react-navigation/native';
import React, { useState, useEffect } from 'react';
import {
  Block,
  Text,
  TouchableOpacity,
  TouchableOpacityProps,
} from '../../common';
import { FormLayout } from '../layout';
import { FormTitle, FormTitleProps } from '../title';

type Option = { value: any; title: string };
interface SelectboxHorizontalContainerItemProps extends TouchableOpacityProps {
  option: Option;

  selected?: boolean | undefined;
}
function SelectboxHorizontalContainerItem({
  selected,
  option: { title, value },
  ...rest
}: SelectboxHorizontalContainerItemProps) {
  const { colors } = useTheme() as AppTheme;

  const b_size = 20;
  const s_size = 12;

  return (
    <TouchableOpacity
      flex
      row
      alignCenter
      justifyCenter
      padding={[8, 8]}
      color={selected ? colors.primary : null}
      {...rest}
    >
      <Text margin={[0, 6]} color={selected ? colors.white : colors.text}>
        {title}
      </Text>
    </TouchableOpacity>
  );
}

export interface SelectboxHorizontalContainerProps {
  options: Array<Option>;
  onChangeValue?: (selected: Option | null) => void;
  // title
  title?: string;
  titleProps?: FormTitleProps;

  // warning
  warning?: string | undefined;
}

export function SelectboxHorizontalContainerBase({
  options,
  onChangeValue,
}: SelectboxHorizontalContainerProps) {
  // theme
  const { colors } = useTheme() as AppTheme;

  // state
  const [selected, setSelected] = useState<Option | null>(null);

  // lifecycles
  useEffect(() => {
    if (onChangeValue) onChangeValue(selected);
  }, [selected]);

  return (
    <Block
      row
      around
      width={'100%'}
      borderRadius={8}
      borderWidth
      borderColor
      color={colors.white}
      style={{ overflow: 'hidden' }}
    >
      {options.map((option, index) => (
        <SelectboxHorizontalContainerItem
          key={index.toString()}
          option={option}
          selected={selected ? option.value === selected.value : false}
          onPress={() => setSelected(option)}
          borderLeftWidth={index ? 1 : 0}
          borderColor
        />
      ))}
    </Block>
  );
}

export function SelectboxHorizontalContainer({
  title,
  titleProps,
  options,
  warning,
  onChangeValue,
  ...rest
}: SelectboxHorizontalContainerProps) {
  // theme
  const { colors } = useTheme() as AppTheme;

  // state
  const [selected, setSelected] = useState<Option | null>(null);

  // lifecycles
  useEffect(() => {
    if (onChangeValue) onChangeValue(selected);
  }, [selected]);

  return (
    <FormLayout
      title={title}
      titleProps={titleProps}
      CenterComponent={SelectboxHorizontalContainerBase}
      centerComponentProps={{
        autoCapitalize: 'none',
        options,
        onChangeValue,
      }}
      warning={warning}
      warningProps={{
        color: 'blue',
      }}
      {...rest}
    />
  );
}
