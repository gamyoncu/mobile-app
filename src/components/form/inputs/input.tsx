import React, { useState, useEffect } from 'react';
import { useTheme } from '@react-navigation/native';
import { TextInput, TextInputProps } from 'react-native';
// import { TextInput, TextInputProps } from '../../common';
import { FormLayout, FormLayoutProps } from '../layout';
import { UseFormRegisterReturn } from 'react-hook-form';

export interface InputProps extends FormLayoutProps {
  textInputProps?: TextInputProps;
  useForm?: UseFormRegisterReturn;

  defaultValue?: string;
  // control?: Control;
  // name: FieldName<FieldValues>;
}

export function Input({
  title,
  titleProps,

  warning,
  warningProps,

  textInputProps: { onChangeText, ...restTextInputProps },
  useForm,
  defaultValue = '',
  // control,
  // name,

  style,

  rightIconName,
  ...rest
}: InputProps) {
  // theme
  const { colors } = useTheme() as AppTheme;

  // // form
  // const { field } = useController({
  //   control,
  //   defaultValue,
  //   name,
  // });

  // state
  const [secure, setSecure] = useState<boolean>(
    (restTextInputProps && restTextInputProps.secureTextEntry) || false
  );
  const [value, setValue] = useState('');

  useEffect(() => {
    if (restTextInputProps?.value) setValue(restTextInputProps?.value);
  }, [restTextInputProps]);

  return (
    <FormLayout
      title={title}
      titleProps={titleProps}
      CenterComponent={TextInput}
      value={value}
      centerComponentProps={{
        autoCapitalize: 'none',
        ...useForm,
        secureTextEntry: secure,
        onChangeText: (text: string) => {
          setValue(text);
        },
        ...restTextInputProps,
      }}
      rightIconName={
        rightIconName
          ? rightIconName
          : restTextInputProps && restTextInputProps.secureTextEntry
          ? secure
            ? 'eye-slash'
            : 'eye'
          : null
      }
      rightIconTouchable
      rightIconProps={{
        onPress: () => setSecure((prev: boolean) => !prev),
      }}
      rightIconColor={
        restTextInputProps && restTextInputProps.secureTextEntry && !secure
          ? colors['high-emphasis']
          : null
      }
      warning={warning}
      {...rest}
    />
  );
}
