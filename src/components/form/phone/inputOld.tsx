import React, { useState } from 'react';
import { useTheme } from '@react-navigation/native';
import PhoneInput from 'react-native-phone-input';
import { Block, Text, TextProps } from '../../common';
import { FormTitle, FormTitleProps } from '../title';
// import CountryPicker from 'react-native-country-picker-modal';
import { TextInput } from 'react-native';
import { CountryPicker } from '../countryPicker';

export interface InputPhoneProps {
  title?: string;
  titleProps?: FormTitleProps;

  warning?: string;
  warningProps?: TextProps;
}

export function InputPhone({
  title,
  titleProps,

  warning,
  warningProps,
}: InputPhoneProps) {
  // theme
  const { colors } = useTheme() as AppTheme;

  // state
  const [cca2, setCca2] = useState(null);

  // refs
  let countryPicker: any;
  let phone: any;

  const onPressFlag = () => {
    countryPicker.openModal();
  };

  const selectCountry = (country: any) => {
    if (phone) phone.selectCountry(country.cca2.toLowerCase());
    setCca2(country.cca2);
  };

  return (
    <Block margin={[4, 0]}>
      <FormTitle {...titleProps}>{title}</FormTitle>
      <PhoneInput
        style={{
          width: '100%',
          backgroundColor: colors.card,
          padding: 8,
          borderWidth: 1,
          borderColor: colors.border,
          borderRadius: 12,
        }}
        ref={(ref) => (phone = ref)}
        onPressFlag={onPressFlag}
        cancelText={'Cancel'}
        confirmText={'OK'}
        initialCountry={'us'}
      />
      <Text h6 color={colors.secondary} margin={[2, 0, 0, 0]} {...warningProps}>
        {warning}
      </Text>
      <CountryPicker
        modalProps={{
          visible: false,
        }}
        onSelect={() => {}}
        countryCode={'TR'}
      />
    </Block>
  );
}
