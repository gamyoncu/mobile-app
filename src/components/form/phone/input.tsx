import React, { useState } from 'react';
import { useTheme } from '@react-navigation/native';
import { TextInput, TextInputProps } from 'react-native';
// import { TextInput, TextInputProps } from '../../common';
import { FormLayout, FormLayoutProps } from '../layout';
import { UseFormRegisterReturn } from 'react-hook-form';
import { CountryPicker } from '../countryPicker';
import { Country, CountryList } from '../countryPicker/src';
import { Block, Text } from '../../common';

export interface InputPhoneProps extends FormLayoutProps {
  textInputProps?: TextInputProps;
  useForm?: UseFormRegisterReturn;

  defaultValue?: string;
  // control?: Control;
  // name: FieldName<FieldValues>;
}

export function InputPhone({
  title,
  titleProps,

  warning,
  warningProps,

  textInputProps,
  useForm,
  defaultValue = '',
  // control,
  // name,

  style,

  rightIconName,
  ...rest
}: InputPhoneProps) {
  // theme
  const { colors } = useTheme() as AppTheme;

  const [country, setCountry] = useState<Country | null>(null);
  const [visibleCountryPicker, setVisibleCountryPicker] = useState(false);

  return (
    <FormLayout
      title={title}
      titleProps={titleProps}
      CenterComponent={TextInput}
      centerComponentProps={{
        autoCapitalize: 'none',
        // value: field.value,
        // onChangeText: field.onChange,
        ...textInputProps,
        ...useForm,
        value: textInputProps?.value,
      }}
      left={
        <Block row alignCenter>
          <CountryPicker
            modalProps={{
              visible: visibleCountryPicker,
            }}
            countryCode={country?.cca2 || 'TR'} // TODO change default country code
            withCallingCode
            withFlagButton
            withFilter
            withAlphaFilter
            onOpen={() => setVisibleCountryPicker(true)}
            onClose={() => setVisibleCountryPicker(false)}
            onSelect={(country: Country) => setCountry(country)}
          />
          <Text>+{country?.callingCode || '90'}</Text>
        </Block>
      }
      warning={warning}
      warningProps={{
        color: 'blue',
      }}
      {...rest}
    />
  );
}
