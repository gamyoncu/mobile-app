import React from 'react';
import CountryPickerBase, { CountryPickerBaseProps } from './src/';
import { CountryCode, Country } from './src/types';
import { Row } from './src/Row';
import { DARK_THEME } from './src/CountryTheme';

export interface CountryPickerProps extends CountryPickerBaseProps {}

export function CountryPicker({
  modalProps = {
    visible: false,
  },
  // countryCode,
  // withFilter = true,
  // // excludeCountries: ['FR'],
  // withFlag = true,
  // withFlagButton = true,
  // withCurrencyButton = false,
  // withCallingCodeButton = true,
  // withCountryNameButton = false,
  // withAlphaFilter = false,
  // withCallingCode = false,
  // withCurrency = false,
  // withEmoji = true,
  // withModal = true,
  // onSelect,
  // disableNativeModal = false,

  // theme
  ...rest
}: CountryPickerProps) {
  return (
    <CountryPickerBase
      {...{
        preferredCountries: ['US', 'GB'],
        modalProps,
      }}
      {...rest}
    />
  );
}
