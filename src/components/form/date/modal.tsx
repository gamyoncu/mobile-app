import React, { useState } from 'react';
import { Modal, ModalProps, Block, Text } from '../../common';
import DateTimePicker from '@react-native-community/datetimepicker';
import { ButtonContainer } from '../../buttons';
import { useTheme } from '@react-navigation/native';

export interface DateModalProps extends ModalProps {
  title?: string;
  value?: any;
  maximumDate?: Date;
  minimumDate?: Date;
}

export function DateModal({
  // modal base props
  open,
  onPressClose,

  // date spesifix props
  title = '',
  value = new Date(),
  maximumDate = undefined,
  minimumDate = undefined,
}: DateModalProps) {
  // theme
  const { colors } = useTheme() as AppTheme;

  // const [title, setTitle] = useState(value ? moment(value).format('l') : '');
  const [date, setDate] = useState<Date>(new Date(value));

  return (
    <Modal open={open} onPressClose={onPressClose} height={340}>
      <Block
        margin={[0, 0, 32, 0]}
        flex
        color={colors.bg}
        borderRadius
        alignCenter
        justifyCenter
      >
        <DateTimePicker
          testID="dateTimePicker"
          value={date || new Date()}
          mode={'date'}
          is24Hour={true}
          maximumDate={maximumDate}
          minimumDate={minimumDate}
          display={'spinner'}
          onChange={(e: any, selectedDate?: Date) => {
            if (selectedDate) setDate(selectedDate);
          }}
          style={{
            height: 200,
            width: '100%',
            backgroundColor: colors.bg,
          }}
        />
        <ButtonContainer
          margin={[0, 0, 32, 0]}
          width={'50%'}
          title={'DONE'}
          onPress={() => {
            if (onPressClose) onPressClose(date);
          }}
        />

        {/* {
          Platform.OS === 'android' && open && (<DateTimePicker
            testID="dateTimePicker"
            value={date}
            mode={'date'}
            is24Hour={true}
            themeVariant={"dark"}
            maximumDate={maximumDate}
            display={"spinner"}
            onChange={(e: any, selectedDate?: Date) => {
              if (selectedDate) {
                setDate(selectedDate)
                setTitle(moment(selectedDate).format('l'));
              }
              if (onPressClose)
                onPressClose(selectedDate);
            }}
            style={{
              height: 200,
              width: '100%'
            }}
          />)
        } */}
      </Block>
    </Modal>
  );
}
