import moment from 'moment';
import React, { Fragment, useState } from 'react';
import { DateModal } from './modal';
import { FormLayout, FormLayoutProps } from '../layout';

export interface InputDateProps extends FormLayoutProps {
  maximumDate?: Date;
  minimumDate?: Date;
  defaultValue?: Date;

  onChangeValue: (date: Date) => void;
}

const formatDate = (result: Date) => moment(result).format('DD/MM/YYYY');

export function InputDate({
  title,
  maximumDate,
  minimumDate,
  defaultValue,
  onChangeValue,
  ...rest
}: InputDateProps) {
  // state
  const [show, setShow] = useState(false);
  const [value, setValue] = useState(
    defaultValue ? formatDate(new Date(defaultValue)) : ''
  );

  return (
    <Fragment>
      <FormLayout
        title={title}
        value={value}
        disabled={false}
        onPress={() => setShow(true)}
        {...rest}
      />
      {show && (
        <DateModal
          open={show}
          onPressClose={(result: any) => {
            if (result) {
              setValue(formatDate(result));
              if (onChangeValue) onChangeValue(result);
            }
            setShow(false);
          }}
          title={title}
          // value={value}
          maximumDate={maximumDate}
          minimumDate={minimumDate}
        />
      )}
    </Fragment>
  );
}
