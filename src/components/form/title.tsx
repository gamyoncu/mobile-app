import { useTheme } from '@react-navigation/native';
import React from 'react';
import { Text, TextProps } from '../common';

export interface FormTitleProps extends TextProps {
  must?: boolean;
}

export function FormTitle({ must, children, ...rest }: FormTitleProps) {
  // theme
  const { colors } = useTheme() as AppTheme;

  if (children)
    return (
      <Text h5 margin={[0, 0, 4, 0]} bold {...rest}>
        {children}
        {must ? <Text color={colors.secondary}>*</Text> : null}
      </Text>
    );
  return null;
}
