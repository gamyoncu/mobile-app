import React from 'react';
import { TouchableOpacity, Block, Text } from '../common';
import { useNavigation, useTheme } from '@react-navigation/native';
import { MaterialIcons } from '@expo/vector-icons';

export interface BackButtonProps {
  title?: string | undefined;
}

export function BackButton({ title }: BackButtonProps) {
  // navigation
  const navigation = useNavigation();

  // theme
  const { colors } = useTheme() as AppTheme;

  return (
    <Block style={{ position: 'absolute', left: 0, padding: 8, zIndex: 1 }}>
      <TouchableOpacity
        row
        alignCenter
        justifyCenter
        onPress={() => navigation.goBack()}
      >
        <MaterialIcons
          name={'chevron-left'}
          size={30}
          color={colors['high-emphasis']}
        />
        <Text fontFamily={'title'}>{title}</Text>
      </TouchableOpacity>
    </Block>
  );
}
