import { Ionicons } from '@expo/vector-icons';
import { useNavigation, useTheme } from '@react-navigation/native';
import React from 'react';
import { Block, BlockProps, Text, TouchableOpacity } from '../common';
import { BackButton } from './backButton';

export interface HeaderProps extends BlockProps {
  leftTitle?: string | undefined;
  title?: string | undefined;
  showBack?: boolean;
  backButtonTitle?: string | undefined;

  showChatIcon?: boolean;
  showDrawerToogleIcon?: boolean;

  right?: any;
  rightIconName?: React.ComponentProps<typeof Ionicons>['name'];
  onPressRightIcon?: () => void;
}

export function Header({
  leftTitle,
  title,
  // backButton Props
  showBack,
  backButtonTitle,
  showChatIcon,
  showDrawerToogleIcon,

  right,
  rightIconName,
  onPressRightIcon,
  ...rest
}: HeaderProps) {
  // theme
  const { colors } = useTheme() as AppTheme;

  // navigation
  const navigation = useNavigation();

  return (
    <Block
      height={38}
      width={'100%'}
      padding={[8, 8, 0, 8]}
      margin={[0, 0, 8, 0]}
      row
      alignCenter
      justifyCenter
      {...rest}
    >
      {leftTitle ? (
        <Text margin={[0, 8]} fontFamily={'title'}>
          {leftTitle}
        </Text>
      ) : null}
      {showBack ? <BackButton title={backButtonTitle} /> : null}
      <Block flex alignCenter justifyCenter>
        <Text fontFamily={'title'}>{title}</Text>
      </Block>
      {right}
      {rightIconName && (
        <TouchableOpacity
          padding={[0, 8, 0, 16]}
          onPress={() => {
            if (onPressRightIcon) onPressRightIcon();
          }}
        >
          <Ionicons name={rightIconName} size={32} color={colors.icon} />
        </TouchableOpacity>
      )}
    </Block>
  );
}
