import React from 'react';
import { Block, BlockProps } from '../common';

export interface BlockBodyProps extends BlockProps {}

export function BlockBody({ ...rest }: BlockBodyProps) {
  return <Block flex padding={[8, 24]} {...rest} />;
}
