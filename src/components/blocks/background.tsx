import { useTheme } from '@react-navigation/native';
import React from 'react';
import { ScrollView } from 'react-native';
import { Block, BlockProps } from '../common';
import { getStatusBarHeight } from '../../constants/layout';

export interface BlockBackgroundProps extends BlockProps {
  canScroll?: boolean | undefined;
  marginTop?: number | undefined;
}

export function BlockBackground({
  canScroll = false,
  marginTop = getStatusBarHeight(false),
  ...rest
}: BlockBackgroundProps) {
  const { colors } = useTheme() as AppTheme;

  const renderBlockBackgrund = () => (
    <Block
      flex
      style={{
        marginTop,
        backgroundColor: colors['main-bg'],
      }}
      {...rest}
    ></Block>
  );

  if (canScroll)
    return (
      <ScrollView
        style={{
          flex: 1,
          backgroundColor: colors['main-bg'],
        }}
      >
        {renderBlockBackgrund()}
      </ScrollView>
    );

  return renderBlockBackgrund();
}
