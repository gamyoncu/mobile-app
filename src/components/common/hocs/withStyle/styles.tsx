import { StyleSheet } from "react-native";

export const BORDER_WIDTH = 1;
export const BORDER_RADIUS = 12;

export const styles = StyleSheet.create({
  flex: {
    flex: 1,
  },
  row: {
    flexDirection: "row",
  },
  column: {
    flexDirection: "column",
  },
  alignCenter: {
    alignItems: "center",
  },
  alignStart: {
    alignItems: "flex-start",
  },
  alignEnd: {
    alignItems: "flex-end",
  },
  justifyCenter: {
    justifyContent: "center",
  },
  justifyStart: {
    justifyContent: "flex-start",
  },
  justifyEnd: {
    justifyContent: "flex-end",
  },
  between: {
    justifyContent: "space-between",
  },
  around: {
    justifyContent: "space-around",
  },
  shadow: {
    // TODO add shadow here
    shadowColor: "rgba(0, 0, 0, 0.27)",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowRadius: 5.8,
    shadowOpacity: 1
  }
});
