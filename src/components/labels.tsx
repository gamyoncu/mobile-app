import { FontAwesome } from '@expo/vector-icons';
import { useTheme } from '@react-navigation/native';
import React from 'react';
import {
  Text,
  Block,
  BlockProps,
  TouchableOpacity,
  TouchableOpacityProps,
} from './common';

interface LabelWIconProps extends TouchableOpacityProps {
  iconName: React.ComponentProps<typeof FontAwesome>['name'];
  title: string;

  reverse?: boolean;
}

export function LabelWIcon({
  iconName,
  title,
  reverse = false,
  disabled = true,
  ...rest
}: LabelWIconProps) {
  // theme
  const { colors } = useTheme() as AppTheme;

  return (
    <TouchableOpacity
      alignCenter
      style={{
        flexDirection: reverse ? 'row-reverse' : 'row',
      }}
      margin={[4, 0]}
      disabled={disabled}
      {...rest}
    >
      <Block width={14} alignCenter justifyCenter>
        <FontAwesome
          name={iconName}
          size={16}
          color={colors['high-emphasis']}
        />
      </Block>
      <Text
        fontFamily={'label'}
        margin={[0, 8]}
        color={colors['high-emphasis']}
        align={'center'}
      >
        {title}
      </Text>
    </TouchableOpacity>
  );
}
