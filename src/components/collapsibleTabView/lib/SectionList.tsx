import React from 'react'
import { SectionList as RNSectionList, SectionListProps } from 'react-native'

import { AnimatedSectionList, IS_IOS } from './helpers'
import {
  useAfterMountEffect,
  useChainCallback,
  useCollapsibleStyle,
  useScrollHandlerY,
  useSharedAnimatedRef,
  useTabNameContext,
  useTabsContext,
  useUpdateScrollViewContentSize,
} from './hooks'

function SectionListImpl<R>(
  {
    contentContainerStyle,
    style,
    onContentSizeChange,
    refreshControl,
    ...rest
  }: Omit<SectionListProps<R>, 'onScroll'>,
  passRef: React.Ref<RNSectionList>
): React.ReactElement {
  const name = useTabNameContext()
  const { setRef, contentInset, scrollYCurrent } = useTabsContext()
  const ref = useSharedAnimatedRef<RNSectionList<unknown>>(passRef)
  const [canBindScrollEvent, setCanBindScrollEvent] = React.useState(false)

  useAfterMountEffect(() => {
    // we enable the scroll event after mounting
    // otherwise we get an `onScroll` call with the initial scroll position which can break things
    setCanBindScrollEvent(true)
  })

  const scrollHandler = useScrollHandlerY(name, { enabled: canBindScrollEvent })
  const {
    style: _style,
    contentContainerStyle: _contentContainerStyle,
    progressViewOffset,
  } = useCollapsibleStyle()

  React.useEffect(() => {
    setRef(name, ref)
  }, [name, ref, setRef])

  const scrollContentSizeChange = useUpdateScrollViewContentSize({
    name,
  })

  const scrollContentSizeChangeHandlers = useChainCallback(
    scrollContentSizeChange,
    onContentSizeChange
  )

  return (
    <AnimatedSectionList
      {...rest}
      // @ts-expect-error problem with reanimated types, they're missing `ref`
      ref={ref}
      bouncesZoom={false}
      style={[_style, style]}
      contentContainerStyle={[_contentContainerStyle, contentContainerStyle]}
      progressViewOffset={progressViewOffset}
      onScroll={scrollHandler}
      onContentSizeChange={scrollContentSizeChangeHandlers}
      scrollEventThrottle={16}
      contentInset={{ top: contentInset }}
      contentOffset={{
        y: IS_IOS ? -contentInset + scrollYCurrent.value : 0,
        x: 0,
      }}
      automaticallyAdjustContentInsets={false}
      refreshControl={
        refreshControl &&
        React.cloneElement(refreshControl, {
          progressViewOffset,
          ...refreshControl.props,
        })
      }
    />
  )
}

/**
 * Use like a regular SectionList.
 */
export const SectionList = React.forwardRef(SectionListImpl) as <T>(
  p: SectionListProps<T> & { ref?: React.Ref<RNSectionList<T>> }
) => React.ReactElement
