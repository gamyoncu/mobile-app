import React, { ReactNode } from 'react';
import { Block, BottomSheetBase } from '../common';
import Animated from 'react-native-reanimated';
// import BottomSheetBase from 'reanimated-bottom-sheet';
import { useTheme } from '@react-navigation/native';

export interface BottomSheetProps {
  snapPoints: Array<number>;
  renderContent: () => ReactNode;
  onCloseEnd: () => void;
}

export class BottomSheet extends React.Component<BottomSheetProps> {
  bottomSheet: any;

  snapTo(index: number) {
    this.bottomSheet.snapTo(index);
  }

  render() {
    return (
      <BottomSheetBase
        ref={(ref: any) => (this.bottomSheet = ref)}
        initialSnap={1}
        borderRadius={10}
        renderHeader={renderHeader}
        {...this.props}
      />
    );
  }
}

const renderHeader = () => {
  // theme
  //   const { colors } = useTheme() as AppTheme;

  return (
    <Block padding={8} alignCenter justifyCenter color={'red'}>
      <Block color={'blue'} style={{ width: 40, height: 3 }} />
    </Block>
  );
};
