import { MaterialIcons } from '@expo/vector-icons';
import { useTheme } from '@react-navigation/native';
import React from 'react';
import { TouchableOpacity, Text, TouchableOpacityProps } from '../components';

export interface DrawerButtonProps extends TouchableOpacityProps {
  iconColor?: string;
  iconName: React.ComponentProps<typeof MaterialIcons>['name'];
  title?: string;
}

export function DrawerButton({
  iconName,
  iconColor,
  title,
  ...rest
}: DrawerButtonProps) {
  // theme
  const { colors } = useTheme() as AppTheme;

  return (
    <TouchableOpacity row alignCenter margin={[8]} {...rest}>
      {iconName && (
        <MaterialIcons
          size={24}
          name={iconName}
          color={iconColor || colors.primary}
        />
      )}
      <Text margin={[0, 8]} h4>
        {title}
      </Text>
    </TouchableOpacity>
  );
}
