import * as React from 'react';
import { useTheme } from '@react-navigation/native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { UserBottomTabNavigator } from './UserTab';
import { Block, Text, TouchableOpacity, Avatar } from '../components';
import { DrawerButton } from './drawerButton';
import { useDispatch, useSelector } from 'react-redux';
import * as reduxActions from '../redux/actions';

const Drawer = createDrawerNavigator<UserDrawerParamList>();

export function UserDrawerNavigator() {
  // theme
  const { colors } = useTheme() as AppTheme;

  // dispatch
  const dispatch = useDispatch();
  const store = useSelector((state) => state) as AppRootState;

  return (
    <Drawer.Navigator
      initialRouteName={'Tab'}
      screenOptions={{
        headerShown: false,
        drawerPosition: 'right',
      }}
      drawerContent={(props) => {
        return (
          <Block flex padding={[36, 0]}>
            <Block margin={[16, 0]} width={'100%'}>
              <TouchableOpacity
                row
                alignCenter
                padding={[0, 8]}
                onPress={() => {
                  props.navigation.navigate('Tab');
                }}
              >
                <Avatar />
                <Text
                  h4
                  margin={[0, 8]}
                  style={{
                    fontFamily: 'monserrat-semibold',
                  }}
                >
                  {store.user.fullname}
                </Text>
              </TouchableOpacity>
            </Block>
            <Block flex borderTopWidth={1} padding={[8]}>
              <DrawerButton
                key={'Edit Profile'}
                title={'Edit Profile'}
                iconName={'edit'}
                onPress={() => props.navigation.navigate('EditProfile')}
              />
              <DrawerButton
                key={'Address'}
                title={'Address'}
                iconName={'location-pin'}
                onPress={() => props.navigation.navigate('Address')}
              />
              <DrawerButton
                key={'Regional'}
                title={'Regional'}
                iconName={'language'}
                onPress={() => props.navigation.navigate('Regional')}
              />
              <DrawerButton
                key={'Change Password'}
                title={'Change Password'}
                iconName={'edit'}
                onPress={() => props.navigation.navigate('ChangePassword')}
              />
            </Block>
            <Block padding={[8]}>
              <DrawerButton
                key={'Settings'}
                title={'Settings'}
                iconName={'settings'}
                onPress={() => props.navigation.navigate('Settings')}
              />
              <DrawerButton
                key={'Logout'}
                title={'Logout'}
                iconName={'logout'}
                iconColor={colors.secondary}
                onPress={() => {
                  // TODO! handle Logout
                  dispatch(reduxActions.auth.logout());
                  props.navigation.closeDrawer();
                }}
              />
            </Block>
          </Block>
        );
      }}
    >
      <Drawer.Screen
        name={'Tab'}
        component={UserBottomTabNavigator}
        options={{
          title: 'Freddik Anderson',
        }}
      />
    </Drawer.Navigator>
  );
}
