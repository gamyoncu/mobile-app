/**
 * If you are not familiar with React Navigation, refer to the "Fundamentals" guide:
 * https://reactnavigation.org/docs/getting-started
 *
 */
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import * as React from 'react';

// screens
import { BoardingScreen } from '../screens/authentication/Boarding/BoardingScreen';
import { LoginScreen } from '../screens/authentication/Login/LoginScreen';
import { SignupScreen } from '../screens/authentication/Signup/SignupScreen';
import { ForgotPasswordScreen } from '../screens/authentication/ForgotPassword/ForgotPasswordScreen';
import { WebviewScreen } from '../screens/shared/Webview/WebviewScreen';
import { SignupPersonalScreen } from '../screens/authentication/Signup/PersonalScreen';
import { SignupBusinessScreen } from '../screens/authentication/Signup/BusinessScreen';
import { SignupAddressScreen } from '../screens/authentication/Signup/AddressScreen';

/**
 * A root stack navigator is often used for displaying modals on top of all other content.
 * https://reactnavigation.org/docs/modal
 */
const Stack = createNativeStackNavigator<AuthNavigationParamList>();

export function AuthNavigator() {
  return (
    <Stack.Navigator initialRouteName={'Login'}>
      <Stack.Screen
        name={'Boarding'}
        component={BoardingScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name={'Signup'}
        component={SignupScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name={'Login'}
        component={LoginScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name={'SignupPersonal'}
        component={SignupPersonalScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name={'SignupBusiness'}
        component={SignupBusinessScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name={'SignupAddress'}
        component={SignupAddressScreen}
        options={{ headerShown: false }}
      />

      <Stack.Screen
        name={'ForgotPassword'}
        component={ForgotPasswordScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name={'Webview'}
        component={WebviewScreen}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
}
