/**
 * If you are not familiar with React Navigation, refer to the "Fundamentals" guide:
 * https://reactnavigation.org/docs/getting-started
 *
 */
import { NavigationContainer } from '@react-navigation/native';
import React, { Fragment, useEffect } from 'react';
import { ColorSchemeName } from 'react-native';

import { AUTH_NAVIGATION_CONFIG } from './LinkingConfiguration';

// theme
import { DarkTheme, DefaultTheme } from '../constants/theme';

// navigators
import { AuthNavigator } from './Auth';
// import { UserStackNavigator } from './UserStack';
import {
  useDispatch,
  // useDispatch,
  useSelector,
} from 'react-redux';
import { UserStackNavigator } from './UserStack';
import { Loading } from '../components';

// screens
import { ForceUpdateScreen } from '../screens/shared/ForceUpdate/ForceUpdateScreen';
import { SendAdvertModal } from '../screens/user/SendAdvert/SendAdvertModal';

function RootRoute() {
  const {
    app: { firstOpen, forceUpdate },
    auth: { token, userTypeId },
    status,
  } = useSelector((state) => state) as RootState;

  useEffect(() => {
    // dispatch(reduxActions.app.init());
  }, []);

  if (status.init) return <Loading />;

  if (forceUpdate) return <ForceUpdateScreen />;

  return (
    <Fragment>
      <UserStackNavigator />
      <SendAdvertModal />
    </Fragment>
  );
  if (token) return <UserStackNavigator />;

  return (
    <Fragment>
      <AuthNavigator />
    </Fragment>
  );
}

export default function Navigation({
  colorScheme,
}: {
  colorScheme: ColorSchemeName;
}) {
  return (
    <NavigationContainer
      linking={AUTH_NAVIGATION_CONFIG}
      theme={colorScheme === 'dark' ? DarkTheme : DefaultTheme}
    >
      <RootRoute />
    </NavigationContainer>
  );
}
