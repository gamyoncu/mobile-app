/**
 * If you are not familiar with React Navigation, refer to the "Fundamentals" guide:
 * https://reactnavigation.org/docs/getting-started
 *
 */
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import * as React from 'react';
import { UserDrawerNavigator } from './UserDrawerNavigator';

/**
 * A root stack navigator is often used for displaying modals on top of all other content.
 * https://reactnavigation.org/docs/modal
 */
const Stack = createNativeStackNavigator<UserStackParamList>();

export function UserStackNavigator() {
  return (
    <Stack.Navigator
      initialRouteName={'Address'}
      screenOptions={{
        headerShown: false,
      }}
    >
      <Stack.Screen name="Root" component={UserDrawerNavigator} />
    </Stack.Navigator>
  );
}
