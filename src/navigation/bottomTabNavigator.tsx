import { FontAwesome } from '@expo/vector-icons';
import { BottomTabBarProps } from '@react-navigation/bottom-tabs';
import { useTheme } from '@react-navigation/native';
import React, { Fragment } from 'react';
import { View } from 'react-native';
import { useDispatch } from 'react-redux';
import { Block, TouchableOpacity, Text } from '../components';
import { LAYOUT } from '../constants/layout';
import * as reduxActions from '../redux/actions';

export interface MyTabBarProps extends BottomTabBarProps {
  renderIcon?: (name: string, isFocused: boolean) => JSX.Element | null;
}

export function MyTabBar({
  state,
  descriptors,
  navigation,
  renderIcon,
}: MyTabBarProps) {
  // colors
  const { colors } = useTheme() as AppTheme;

  // dispatch
  const dispatch = useDispatch();

  return (
    <View
      style={{
        flexDirection: 'row',
        backgroundColor: colors['main-bg'],
        justifyContent: 'center',
        alignItems: 'center',
        height: 100,
      }}
    >
      <Block
        row
        color={colors['surface-primary']}
        shadow
        height={72}
        margin={[0, 0, 24, 0]}
        width={LAYOUT.window.width - 48}
        borderRadius={24}
        around
        style={{
          position: 'absolute',
          bottom: 24,
          shadowColor: 'rgba(86, 88, 238, 0.1)',
          shadowOffset: {
            width: 0,
            height: -5,
          },
          shadowRadius: 10,
          shadowOpacity: 1,
        }}
      >
        {state.routes.map((route, index) => {
          const { options } = descriptors[route.key];
          const label =
            options.tabBarLabel !== undefined
              ? options.tabBarLabel
              : options.title !== undefined
              ? options.title
              : route.name;

          const isFocused = state.index === index;

          const onPress = () => {
            const event = navigation.emit({
              type: 'tabPress',
              target: route.key,
            });

            if (!isFocused && !event.defaultPrevented) {
              navigation.navigate(route.name);
            }
          };

          const onLongPress = () => {
            navigation.emit({
              type: 'tabLongPress',
              target: route.key,
            });
          };

          return (
            <Fragment key={route.name}>
              <TouchableOpacity
                accessibilityRole="button"
                accessibilityLabel={options.tabBarAccessibilityLabel}
                testID={options.tabBarTestID}
                onPress={onPress}
                onLongPress={onLongPress}
                // style={{ flex: 1, alignItems: 'center', bottom: 8 }}
                justifyCenter
                alignCenter
              >
                {renderIcon && renderIcon(route.name, isFocused)}
                <Text
                  fontFamily={'label'}
                  margin={[4, 0]}
                  color={
                    isFocused ? colors['high-emphasis'] : colors['low-emphasis']
                  }
                  style={{ textAlign: 'center' }}
                >
                  {label}
                </Text>
              </TouchableOpacity>
              {route.name === 'Feed' ? (
                <TouchableOpacity
                  color={colors.primary}
                  width={76}
                  height={76}
                  borderRadius={38}
                  justifyCenter
                  alignCenter
                  style={{
                    position: 'relative',
                    zIndex: 1,
                    bottom: 24,
                    shadowColor: 'rgba(46, 95, 227, 0.4)',
                    shadowOffset: {
                      width: 0,
                      height: 0,
                    },
                    shadowRadius: 50,
                    shadowOpacity: 1,
                  }}
                  onPress={() =>
                    dispatch(
                      reduxActions.app.changeReducer({ openSendAdvert: true })
                    )
                  }
                >
                  <FontAwesome
                    size={24}
                    color={colors['surface-primary']}
                    name={'send'}
                  />
                  <Text
                    fontFamily={'label'}
                    margin={[4, 0]}
                    color={colors['surface-primary']}
                    style={{ textAlign: 'center' }}
                  >
                    İlan Ver
                  </Text>
                </TouchableOpacity>
              ) : null}
            </Fragment>
          );
        })}
      </Block>
    </View>
  );
}
