/**
 * Learn more about deep linking with React Navigation
 * https://reactnavigation.org/docs/deep-linking
 * https://reactnavigation.org/docs/configuring-links
 */

import { LinkingOptions } from '@react-navigation/native';
import * as Linking from 'expo-linking';
import { BottomTabScreenProps } from '@react-navigation/bottom-tabs';
import {
  CompositeScreenProps,
  NavigatorScreenParams,
  DrawerActionHelpers,
  StackActionHelpers,
} from '@react-navigation/native';
import { NativeStackScreenProps } from '@react-navigation/native-stack';

declare global {
  namespace ReactNavigation {
    interface RootParamList
      extends AuthNavigationParamList,
        StackActionHelpers<AuthNavigationParamList> {}
    interface RootParamList
      extends UserStackParamList,
        DrawerActionHelpers<UserDrawerParamList>,
        StackActionHelpers<AuthNavigationParamList> {}
  }

  type AuthNavigationParamList = {
    Boarding: undefined;
    Login: undefined;

    Signup: undefined;
    SignupPersonal: undefined;
    SignupLocalization: undefined;
    SignupBusiness: undefined;
    SignupAddress: undefined;

    ForgotPassword: undefined;
    Webview: undefined;
  };

  type AddressAddScreenParamList = {
    title: string;
    item?: AddressProps;
  };

  // User
  type UserStackParamList = {
    Root: NavigatorScreenParams<UserDrawerParamList> | undefined;
    EditProfile: undefined;
    Settings: undefined;
    Regional: undefined;
    ChangePassword: undefined;

    Webview: undefined;
    ChatList: undefined;
    Chat: undefined;

    Address: undefined;
    AddressAdd: AddressAddScreenParamList;
  };

  type UserStackScreenProps<Screen extends keyof UserStackParamList> =
    NativeStackScreenProps<UserStackParamList, Screen>;

  type UserDrawerParamList = {
    Tab: NavigatorScreenParams<UserTabParamList> | undefined;
  };

  type UserDrawerScreenProps<Screen extends keyof UserDrawerParamList> =
    CompositeScreenProps<
      NativeStackScreenProps<UserDrawerParamList, Screen>,
      NativeStackScreenProps<UserStackParamList>
    >;

  type UserTabParamList = {
    Feed: undefined;
    Profile: undefined;
  };

  type UserTabScreenProps<Screen extends keyof UserTabParamList> =
    CompositeScreenProps<
      BottomTabScreenProps<UserTabParamList, Screen>,
      NativeStackScreenProps<UserDrawerParamList>
    >;

  // end User
}

export const AUTH_NAVIGATION_CONFIG: LinkingOptions<AuthNavigationParamList> = {
  prefixes: [Linking.makeUrl('/')],
  config: {
    screens: {
      Boarding: 'Boarding',
      Login: 'Login',

      Signup: 'Signup',
      SignupPersonal: 'SignupPersonal',
      SignupLocalization: 'SignupLocalization',
      SignupBusiness: 'SignupBusiness',
      SignupAddress: 'SignupAddress',

      ForgotPassword: 'ForgotPassword',
      Webview: 'Webview',
    },
  },
};

export const User_NAVIGATION_CONFIG: LinkingOptions<UserStackParamList> = {
  prefixes: [Linking.makeUrl('/')],
  config: {
    screens: {
      Root: {
        screens: {
          Tab: {
            screens: {
              Home: {
                screens: {
                  TabOneScreen: 'one',
                },
              },
              Teams: {
                screens: {
                  TabTwoScreen: 'two',
                },
              },
              Booking: {
                screens: {
                  TabTwoScreen: 'two',
                },
              },
              TestCenter: {
                screens: {
                  TabTwoScreen: 'two',
                },
              },
              VideoAnalysis: {
                screens: {
                  TabTwoScreen: 'two',
                },
              },
            },
          },
        },
      },
      Webview: 'Webview',
    },
  },
};
