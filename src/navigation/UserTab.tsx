/**
 * If you are not familiar with React Navigation, refer to the "Fundamentals" guide:
 * https://reactnavigation.org/docs/getting-started
 *
 */
import { FontAwesome } from '@expo/vector-icons';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { useTheme } from '@react-navigation/native';
import * as React from 'react';
import { FeedScreen } from '../screens/user/Feed/FeedScreen';
import { ProfileScreen } from '../screens/user/Profile/ProfileScreen';
import { MyTabBar } from './bottomTabNavigator';

/**
 * A bottom tab navigator displays tab buttons on the bottom of the display to switch screens.
 * https://reactnavigation.org/docs/bottom-tab-navigator
 */
const BottomTab = createBottomTabNavigator<UserTabParamList>();

export function UserBottomTabNavigator() {
  // colors
  const { colors } = useTheme() as AppTheme;

  return (
    <BottomTab.Navigator
      initialRouteName={'Profile'}
      tabBar={(props) => (
        <MyTabBar
          renderIcon={(name: string, isFocused: boolean) => {
            if (name === 'Feed') {
              return (
                <TabBarIcon
                  name="home"
                  color={
                    isFocused ? colors['high-emphasis'] : colors['low-emphasis']
                  }
                />
              );
            }
            if (name === 'Profile') {
              return (
                <TabBarIcon
                  name={'user'}
                  color={
                    isFocused ? colors['high-emphasis'] : colors['low-emphasis']
                  }
                />
              );
            }
            return null;
          }}
          {...props}
        />
      )}
      screenOptions={{
        headerShown: false,
      }}
    >
      <BottomTab.Screen
        name={'Feed'}
        component={FeedScreen}
        options={{
          title: 'İlanlar',
        }}
      />
      <BottomTab.Screen
        name={'Profile'}
        component={ProfileScreen}
        options={{
          title: 'Hesabım',
        }}
      />
    </BottomTab.Navigator>
  );
}

/**
 * You can explore the built-in icon families and icons on the web at https://icons.expo.fyi/
 */
function TabBarIcon(props: {
  name: React.ComponentProps<typeof FontAwesome>['name'];
  color: string;
}) {
  return <FontAwesome size={24} style={{}} {...props} />;
}
