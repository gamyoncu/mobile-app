import { setItem, getItem } from './base';

const KEY = 'localization';

/**
 *  get
 *
 * @export
 * @return {Promise} 
 */
export async function get() {
    return await getItem(KEY)
}

/**
 *  set
 *
 * @export
 * @param {string} value
 * @return {Promise<string>} 
 */
export async function set(value) {
    return await setItem(KEY, value)
}