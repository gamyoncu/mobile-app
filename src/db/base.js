import AsyncStorage from '@react-native-async-storage/async-storage';

/**
 *  setItem
 *
 * @export
 * @param {string} key
 * @param {any} value
 */
export async function setItem(key, value) {
    try {
        const strValue = JSON.stringify(value);
        await AsyncStorage.setItem(key, strValue)
    } catch (error) {
        console.warn('ERROR :: DB ::', key, ' :: ', error)
    }
}

/**
 *  getItem
 *
 * @export
 * @param {string} key
 * @return {Promise<any>}
 */
export async function getItem(key) {
    var value = null;
    try {
        const res = await AsyncStorage.getItem(value)
        value = JSON.parse(res);
    } catch (error) {
        console.warn('ERROR :: DB ::', key, ' :: ', error)
    }
    return value;
}