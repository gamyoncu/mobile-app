import React from 'react';
import { useTheme, useNavigation } from '@react-navigation/native';

import {
  Block,
  BlockBackground,
  BlockBody,
  ButtonContainer,
  ButtonText,
  Image,
  Text,
} from '../../../components';
import { LAYOUT } from '../../../constants/layout';

export function BoardingScreen({}) {
  // theme
  const { colors } = useTheme() as AppTheme;

  // navigation
  const navigation = useNavigation();

  return (
    <BlockBackground flex>
      <BlockBody alignCenter>
        <Image source={require('../../../assets/images/boarding.png')} />
        <Text fontFamily={'h3'} align={'center'}>
          Hoşgeldin
        </Text>
        <Text margin={[16, 0]} fontFamily={'label'} align={'center'}>
          Gamyoncu app ile yükünü taşıtmak istediğin taşımacılara ulaşabilir,
          ilanları inceleyebilir ve filtre özelliğini kullanarak sana uygun
          ilanı zaman kaybetmeden bulabilirsin!
        </Text>
        <ButtonContainer
          width={'100%'}
          iconName={'phone'}
          title={'Telefonla kayıt ol'}
        />
        <Block row justifyCenter between>
          <ButtonContainer
            width={LAYOUT.window.width / 2 - 48}
            iconName={'facebook-square'}
            title={'Facebook'}
          />
          <ButtonContainer
            width={LAYOUT.window.width / 2 - 48}
            iconName={'apple'}
            title={'Apple'}
          />
        </Block>
        <ButtonText
          margin={[16, 0]}
          theme={'gray'}
          title={'Üye olmadan devam et'}
        />
        <Block margin={[16, 0]}>
          <Text fontFamily={'caption'} align={'center'}>
            Hesabın var mı?
          </Text>
          <ButtonText
            theme={'primary'}
            title={'Giriş yap'}
            onPress={() => navigation.navigate('Login')}
          />
        </Block>
      </BlockBody>
    </BlockBackground>
  );
}
