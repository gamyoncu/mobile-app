import React from 'react';
import { useNavigation } from '@react-navigation/native';

import {
  Block,
  BlockBackground,
  BlockBody,
  ButtonContainer,
  ButtonText,
  ButtonWIcon,
  Header,
  Input,
  Text,
} from '../../../components';

// form
import { Controller, useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { useDispatch, useSelector } from 'react-redux';
import * as reduxActions from '../../../redux/actions';

export function LoginScreen({}) {
  // navigation
  const navigation = useNavigation();

  // dispatch
  const dispatch = useDispatch();
  const store = useSelector((state) => state) as AppRootState;

  // form
  const {
    control,
    getValues,
    formState: { errors },
    handleSubmit,
  } = useForm({
    resolver: yupResolver(
      yup
        .object({
          email: yup
            .string()
            .default('')
            .email('Telefon girmelisiniz')
            .required(),
          password: yup
            .string()
            .default('')
            .test('len', 'En az 4 karakter olmalı', (val) => val.length >= 4),
        })
        .required()
    ),
  });

  const onSubmit = (data: LoginReq) => {
    dispatch(reduxActions.auth.login(data));
  };

  return (
    <BlockBackground flex>
      <Header showBack title={'Giriş Yap'} />
      <BlockBody flex justifyCenter>
        <Block alignCenter>
          <Controller
            control={control}
            name="email"
            rules={{
              required: true,
            }}
            render={({ field: { onChange, ...rest } }) => (
              <Input
                textInputProps={{
                  placeholder: '+90 Cep Telefonu',
                  onChangeText: (e) => onChange(e),
                  ...rest,
                }}
                warning={errors.email?.message}
              />
            )}
          />
          <Controller
            control={control}
            name="password"
            rules={{
              required: true,
            }}
            render={({ field: { onChange, ...rest } }) => (
              <Input
                textInputProps={{
                  placeholder: 'Şifre',
                  secureTextEntry: true,
                  onChangeText: (e) => onChange(e),
                  ...rest,
                }}
                warning={errors.password?.message}
              />
            )}
          />
        </Block>
        <ButtonText
          theme={'primary'}
          title={'Forgot Password?'}
          onPress={() => navigation.navigate('ForgotPassword')}
        />
        <ButtonContainer
          title={'Giriş Yap'}
          loading={store.status.register}
          onPress={handleSubmit(onSubmit)}
        />
      </BlockBody>
    </BlockBackground>
  );
}
