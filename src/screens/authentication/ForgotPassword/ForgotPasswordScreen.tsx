import React from 'react';
import { useTheme, useNavigation } from '@react-navigation/native';
import {
  Block,
  BlockBackground,
  BlockBody,
  ButtonContainer,
  ButtonWIcon,
  Header,
  Input,
  Text,
} from '../../../components';

export function ForgotPasswordScreen({}) {
  // theme
  const { colors } = useTheme() as AppTheme;

  // navigation
  const navigation = useNavigation();

  const onSubmit = () => {};

  return (
    <BlockBackground flex>
      <Header showBack title={'Şifremi unuttum'} />
      <BlockBody flex justifyCenter>
        <Block alignCenter>
          <Input
            textInputProps={{
              placeholder: 'john@mail.com',
            }}
          />
        </Block>
        <Block alignCenter>
          <ButtonContainer title={'Gönder'} />
        </Block>
      </BlockBody>
    </BlockBackground>
  );
}
