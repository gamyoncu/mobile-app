import React from 'react';
import { useTheme } from '@react-navigation/native';

import {
  Block,
  BlockBackground,
  BlockBody,
  ButtonWIcon,
  Input,
  Text,
  ButtonText,
  InputPicker,
} from '../../../components';

export function SignupBusinessScreen({}) {
  // theme
  const { colors } = useTheme();
  return (
    <BlockBackground flex>
      <BlockBody flex>
        <Text h2 margin={[8, 0]} font={'monserrat-semibold'}>
          Business Information
        </Text>
        <Block alignCenter>
          <InputPicker title={'County'} />
          <Input
            title={'Home Club'}
            textInputProps={{
              placeholder: 'Doe',
            }}
            rightIconName={'search'}
          />
          <ButtonWIcon title={'Continue'} />
        </Block>
      </BlockBody>
      <Block margin={[24, 0]} alignCenter>
        <ButtonText title={'SKIP'} />
      </Block>
    </BlockBackground>
  );
}
