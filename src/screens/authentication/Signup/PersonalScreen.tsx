import React from 'react';
import { useTheme, useNavigation } from '@react-navigation/native';

import {
  Block,
  BlockBackground,
  BlockBody,
  ButtonWIcon,
  Text,
  ButtonText,
  InputDate,
  ButtonAddAvatarPhoto,
  InputPicker,
} from '../../../components';
import { GENDER_OPTIONS } from '../../../constants/enums';

export function SignupPersonalScreen({}) {
  // theme
  const { colors } = useTheme();

  // navigation
  const navigation = useNavigation();

  return (
    <BlockBackground flex>
      <BlockBody flex>
        <Text h2 margin={[8, 0]} font={'monserrat-semibold'}>
          Personal Information
        </Text>
        <Block alignCenter>
          <ButtonAddAvatarPhoto />
          <InputPicker
            title={'Gender'}
            titleProps={{
              must: true,
            }}
            options={GENDER_OPTIONS}
          />
          <InputDate
            title={'Birthday'}
            titleProps={{
              must: true,
            }}
            maximumDate={new Date()}
          />
          <ButtonWIcon
            title={'Continue'}
            onPress={() => navigation.navigate('SignupBusiness')}
          />
        </Block>
      </BlockBody>
      <Block margin={[24, 0]} alignCenter>
        <ButtonText
          title={'SKIP'}
          onPress={() => navigation.navigate('SignupBusiness')}
        />
      </Block>
    </BlockBackground>
  );
}
