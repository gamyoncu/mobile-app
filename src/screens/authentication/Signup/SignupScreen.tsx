import React from 'react';
import { useNavigation } from '@react-navigation/native';

import {
  Block,
  BlockBackground,
  BlockBody,
  ButtonWIcon,
  Header,
  Input,
  Text,
  SelectboxHorizontalContainer,
  ButtonText,
} from '../../../components';
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { useDispatch, useSelector } from 'react-redux';
import * as reduxActions from '../../../redux/actions';

export function SignupScreen({}) {
  // navigation
  const navigation = useNavigation();

  // redux
  const dispatch = useDispatch();
  const store = useSelector((state) => state) as AppRootState;

  // form
  const {
    control,
    formState: { errors },
    handleSubmit,
  } = useForm({
    defaultValues: {
      userTypeId: 1,
      fullname: '',
      email: '',
      password: '',
    },
    resolver: yupResolver(
      yup
        .object({
          userTypeId: yup.number().required(),
          fullname: yup.string().required(),
          email: yup.string().email().required(),
          password: yup
            .string()
            .test(
              'len',
              'Must be at least 4 characters',
              (val) => val !== '' || val.length >= 4
            ),
        })
        .required()
    ),
  });

  const onRegister = (data: RegisterReq) => {
    dispatch(reduxActions.auth.register(data));
  };

  return (
    <BlockBackground flex>
      <Header showBack backButtonTitle={'Boarding'} />
      <BlockBody flex justifyCenter>
        <Text h1 margin={[8, 0]} font={'monserrat-semibold'}>
          Sign up
        </Text>
        <Block alignCenter>
          <Controller
            control={control}
            name="userTypeId"
            rules={{
              required: true,
            }}
            render={({ field: { onChange, ...rest } }) => (
              <SelectboxHorizontalContainer
                title={'Select User Type'}
                titleProps={{ must: true }}
                options={[
                  {
                    title: 'User',
                    value: 1,
                  },
                  {
                    title: 'Coach',
                    value: 2,
                  },
                ]}
                onChangeValue={(selected) => {
                  if (selected) onChange(selected.value);
                  else onChange(null);
                }}
                warning={errors.userTypeId?.message}
              />
            )}
          />

          <Controller
            control={control}
            name="fullname"
            rules={{
              required: true,
            }}
            render={({ field: { onChange, ...rest } }) => (
              <Input
                title={'Full Name'}
                titleProps={{ must: true }}
                textInputProps={{
                  placeholder: 'John Doe',
                  onChangeText: (e) => onChange(e),
                  ...rest,
                }}
                warning={errors.fullname?.message}
              />
            )}
          />
          <Controller
            control={control}
            name="email"
            rules={{
              required: true,
            }}
            render={({ field: { onChange, ...rest } }) => (
              <Input
                title={'E-mail'}
                titleProps={{ must: true }}
                textInputProps={{
                  placeholder: 'john@mail.com',
                  onChangeText: (e) => onChange(e),
                  ...rest,
                }}
                warning={errors.email?.message}
              />
            )}
          />
          <Controller
            control={control}
            name="password"
            rules={{
              required: true,
            }}
            render={({ field: { onChange, ...rest } }) => (
              <Input
                title={'Password'}
                titleProps={{ must: true }}
                textInputProps={{
                  placeholder: '******',
                  secureTextEntry: true,
                  onChangeText: (e) => onChange(e),
                  ...rest,
                }}
                warning={errors.password?.message}
              />
            )}
          />
          <ButtonWIcon
            title={'Register'}
            loading={store.status.register}
            onPress={handleSubmit(onRegister)}
          />
          <ButtonText
            margin={[16, 0]}
            title={`I already have an account`}
            onPress={() => navigation.navigate('Login')}
          />
        </Block>
      </BlockBody>
    </BlockBackground>
  );
}
