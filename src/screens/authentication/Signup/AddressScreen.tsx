import React from 'react';
import {
  Block,
  BlockBackground,
  BlockBody,
  ButtonWIcon,
  Input,
  Text,
  ButtonText,
  InputPicker,
} from '../../../components';

export function SignupAddressScreen({}) {
  return (
    <BlockBackground flex>
      <BlockBody flex>
        <Text h2 margin={[8, 0]} font={'monserrat-semibold'}>
          Address Information
        </Text>
        <Block alignCenter>
          <Input
            title={'Title'}
            titleProps={{
              must: true,
            }}
            textInputProps={{
              placeholder: 'Home',
            }}
          />
          <InputPicker
            title={'County'}
            titleProps={{
              must: true,
            }}
            placeholder={'Sweden'}
          />
          <InputPicker
            title={'City'}
            titleProps={{
              must: true,
            }}
            placeholder={'Zurich'}
          />
          <Input
            title={'Zip'}
            titleProps={{
              must: true,
            }}
            textInputProps={{
              placeholder: '8001',
            }}
          />
          <Input
            title={'Address Detail'}
            titleProps={{
              must: true,
            }}
            textInputProps={{
              placeholder: '8001',
              multiline: true,
              style: {
                width: '100%',
                minHeight: 80,
              },
            }}
          />
          <ButtonWIcon title={'Continue'} />
        </Block>
      </BlockBody>
      <Block margin={[24, 0]} alignCenter>
        <ButtonText title={'SKIP'} />
      </Block>
    </BlockBackground>
  );
}
