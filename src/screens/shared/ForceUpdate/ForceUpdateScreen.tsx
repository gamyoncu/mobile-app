import React from 'react';
import { Linking, Platform } from 'react-native';
import { DefaultRootState, useSelector } from 'react-redux';
import {
  BlockBackground,
  BlockBody,
  ButtonWIcon,
  Text,
} from '../../../components';
import { makeHandlerAwareOfAsyncErrors } from '../../../helpers';

export function ForceUpdateScreen({}) {
  const {
    app: { iosUrl, androidUrl },
  } = useSelector((state) => state) as AppRootState;

  return (
    <BlockBackground flex>
      <BlockBody alignCenter justifyCenter>
        <Text
          h2
          style={{
            textAlign: 'center',
            margin: 8,
          }}
        >
          There is a new version of application // TODO localization
        </Text>

        <ButtonWIcon
          title={'Open Appstore'} // TODO localization
          onPress={() => {
            makeHandlerAwareOfAsyncErrors(async () => {
              if (Platform.OS === 'ios' && iosUrl)
                await Linking.openURL(iosUrl);
              if (Platform.OS === 'android' && androidUrl)
                await Linking.openURL(androidUrl);
            });
          }}
        />
      </BlockBody>
    </BlockBackground>
  );
}
