import React from 'react';
import { BlockBackground, Header } from '../../../components';
import { WebView } from 'react-native-webview';

export function WebviewScreen({}) {
  return (
    <BlockBackground flex>
      <Header showBack backButtonTitle={'Boarding'} />
      <WebView
        source={{ uri: 'https://www.google.com/' }}
        style={{
          width: '100%',
          height: '100%',
        }}
      />
    </BlockBackground>
  );
}
