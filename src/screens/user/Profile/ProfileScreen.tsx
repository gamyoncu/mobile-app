import React from 'react';
import { useTheme, useNavigation } from '@react-navigation/native';

import {
  Block,
  BlockBackground,
  BlockBody,
  ButtonContainer,
  ButtonText,
  Header,
  Image,
  LabelWIcon,
  Text,
} from '../../../components';
import { LAYOUT } from '../../../constants/layout';
import { Avatar } from '../../../containers';

export function ProfileScreen({}) {
  // theme
  const { colors } = useTheme() as AppTheme;

  // navigation
  const navigation = useNavigation();

  return (
    <BlockBackground flex>
      <BlockBody alignCenter>
        <Header
          right={
            <LabelWIcon
              disabled={false}
              iconName={'edit'}
              title={'Düzenle'}
              reverse
            />
          }
        />
        <Avatar margin={[8, 0]} size={96} />
        <Text
          margin={[8, 0]}
          fontFamily={'title'}
          color={colors['high-emphasis']}
        >
          Hasan Akdoğan
        </Text>
        <Text
          margin={[8, 0]}
          fontFamily={'label'}
          color={colors['medium-emphasis']}
        >
          Akdoğan nakliyat ltd. şti
        </Text>
        <LabelWIcon margin={[8, 0]} iconName={'truck'} title={'Kamyon tipi'} />
        <LabelWIcon
          margin={[8, 0]}
          iconName={'mobile-phone'}
          title={'0 543 939 33 93'}
        />
      </BlockBody>
    </BlockBackground>
  );
}
