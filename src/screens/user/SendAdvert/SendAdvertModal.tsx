import React from 'react';
import { useTheme, useNavigation } from '@react-navigation/native';

import {
  Block,
  BlockBackground,
  BlockBody,
  Modal,
  Input,
  ButtonContainer,
  ButtonText,
  InputDate,
  InputPicker,
} from '../../../components';
import { LAYOUT } from '../../../constants/layout';
import { FontAwesome } from '@expo/vector-icons';
import { useDispatch, useSelector } from 'react-redux';
import * as reduxActions from '../../../redux/actions';

const snapPoints = [0, (3 * LAYOUT.window.height) / 5];

export function SendAdvertModal({}) {
  // theme
  const { colors } = useTheme() as AppTheme;

  // navigation
  const navigation = useNavigation();

  // redux
  const dispatch = useDispatch();
  const {
    app: { openSendAdvert },
  } = useSelector((state) => state) as AppRootState;

  const onCloseModal = () => {
    dispatch(reduxActions.app.changeReducer({ openSendAdvert: false }));
  };

  return (
    <Modal
      open={openSendAdvert}
      height={LAYOUT.window.height > 600 ? 600 : LAYOUT.window.height * 0.8}
      title={'İlan ver'}
      onPressClose={onCloseModal}
    >
      <BlockBackground canScroll>
        <BlockBody>
          <InputPicker
            leftIconName={'address-book'}
            placeholder={'Nereden'}
            options={[]}
          />
          <InputPicker
            leftIconName={'car'}
            placeholder={'Nereye'}
            options={[]}
          />
          <Input
            leftIconName={'truck'}
            textInputProps={{
              placeholder: 'Kamyon tipi',
            }}
          />
          <Input
            leftIconName={'bicycle'}
            textInputProps={{
              placeholder: 'Yükün cinsi',
            }}
          />
          <InputDate
            leftIconName={'calendar'}
            placeholder={'Tarih'}
            minimumDate={new Date()}
            onChangeValue={(date) => {}}
          />

          <Block alignCenter>
            <ButtonContainer title={'İlan ver'} />
            <ButtonText
              margin={[8, 0]}
              titleColor={colors['high-emphasis']}
              title={'İptal'}
              onPress={onCloseModal}
            />
          </Block>
        </BlockBody>
      </BlockBackground>
    </Modal>
  );
}
