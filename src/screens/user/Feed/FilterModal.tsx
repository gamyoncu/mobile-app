import React from 'react';
import { useTheme, useNavigation } from '@react-navigation/native';

import {
  Block,
  BlockBackground,
  BlockBody,
  Modal,
  Input,
  ButtonContainer,
  ButtonText,
  ModalProps,
  InputPicker,
  InputDate,
} from '../../../components';
import { LAYOUT } from '../../../constants/layout';
import { FontAwesome } from '@expo/vector-icons';

export interface FilterModalProps extends ModalProps {}

export function FilterModal({ open, onPressClose }: FilterModalProps) {
  // theme
  const { colors } = useTheme() as AppTheme;

  // navigation
  const navigation = useNavigation();

  return (
    <Modal
      open={open}
      height={LAYOUT.window.height > 450 ? 450 : LAYOUT.window.height * 0.6}
      title={'Lokasyon seç'}
      onPressClose={onPressClose}
    >
      <BlockBackground canScroll>
        <BlockBody>
          <InputPicker
            leftIconName={'address-book'}
            placeholder={'Nereden'}
            options={[]}
          />
          <InputPicker
            leftIconName={'car'}
            placeholder={'Nereye'}
            options={[]}
          />
          <InputDate
            leftIconName={'calendar'}
            placeholder={'Tarih'}
            onChangeValue={(date) => {}}
          />

          <Block alignCenter>
            <ButtonContainer title={'İlan ver'} />
            <ButtonText
              margin={[8, 0]}
              titleColor={colors['high-emphasis']}
              title={'İptal'}
              onPress={() => {
                if (onPressClose) onPressClose();
              }}
            />
          </Block>
        </BlockBody>
      </BlockBackground>
    </Modal>
  );
}
