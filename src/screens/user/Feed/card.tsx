import { useTheme } from '@react-navigation/native';
import React, { Fragment } from 'react';
import { Block, ButtonContainer, LabelWIcon, Text } from '../../../components';
import { Avatar } from '../../../containers';

interface FeedCardProps {}

export function FeedCard(params: FeedCardProps) {
  // theme
  const { colors } = useTheme() as AppTheme;

  return (
    <Block
      width={'100%'}
      margin={[8, 0]}
      color={colors['card-bg']}
      borderRadius={12}
      padding={4}
    >
      <Block padding={8}>
        <Block row>
          <Avatar />
          <Block margin={[0, 8]}>
            <Text fontFamily={'title'} color={colors['high-emphasis']}>
              Hasan Akdoğan
            </Text>
            <Text fontFamily={'label'} color={colors['low-emphasis']}>
              Akdoğan nakliyat ltd. şti
            </Text>
          </Block>
        </Block>
        <Block row between margin={8}>
          <Block>
            <LabelWIcon
              key={'truck'}
              iconName={'truck'}
              title={'Kamyon tipi'}
            />
            <LabelWIcon key={'box'} iconName={'blind'} title={'Yükün cinsi'} />
          </Block>
          <LabelWIcon
            key={'time'}
            iconName={'calendar'}
            title={'24 Mayıs\n Pazartesi'}
            reverse
          />
        </Block>
      </Block>
      <Dash />
      <Block padding={8}>
        <Block row alignCenter margin={[4, 0]}>
          <Block
            width={9}
            height={9}
            borderRadius={4.5}
            color={colors['primary-variant']}
            margin={[0, 0, 0, 8]}
          />
          <Text fontFamily={'label'} color={colors['high-emphasis']}>
            Kalkış yeri: Ankara
          </Text>
        </Block>

        <Block
          style={{
            marginLeft: 4,
            height: 4,
            borderRadius: 1,
            width: 1,
            borderStyle: 'dotted',
            borderWidth: 1,
            borderColor: colors['low-emphasis'],
          }}
        ></Block>
        <Block row alignCenter margin={[4, 0]}>
          <Block
            width={9}
            height={9}
            borderRadius={4.5}
            color={colors['primary']}
            margin={[0, 0, 0, 8]}
          />
          <Text fontFamily={'label'} color={colors['high-emphasis']}>
            Varış yeri: Tokat
          </Text>
        </Block>
      </Block>
      <Dash />
      <Block padding={8} justifyCenter row between>
        <LabelWIcon
          key={'phone'}
          iconName={'phone'}
          title={'+90 543 939 33 93'}
        />
        <ButtonContainer
          height={36}
          margin={0}
          width={'30%'}
          iconName={'phone'}
          title={'Ara'}
        />
      </Block>
    </Block>
  );
}

function Dash() {
  // theme
  const { colors } = useTheme() as AppTheme;

  return (
    <Fragment>
      <Block
        style={{
          marginVertical: 4,
          height: 1,
          borderRadius: 1,
          width: '100%',
          borderStyle: 'dashed',
          borderWidth: 1.2,
          borderColor: 'rgba(161,155,183,0.5)',
        }}
      >
        <Dot position={'left'} />
        <Dot position={'right'} />
      </Block>
    </Fragment>
  );
}

interface DotProps {
  position: 'left' | 'right';
}

function Dot({ position }: DotProps) {
  // theme
  const { colors } = useTheme() as AppTheme;
  return (
    <Block
      width={12}
      height={12}
      borderRadius={6}
      //   color={colors['main-bg']}
      style={[
        {
          position: 'absolute',

          zIndex: 99,
          backgroundColor: colors.bg,
        },
        position === 'left'
          ? {
              left: -12,
              bottom: -6,
            }
          : {
              right: -12,
              bottom: -6,
            },
      ]}
    ></Block>
  );
}
