import React, { useState } from 'react';
import { useTheme, useNavigation } from '@react-navigation/native';

import {
  Block,
  BlockBackground,
  BlockBody,
  ButtonContainer,
  ButtonText,
  Header,
  Image,
  Text,
  TouchableOpacity,
} from '../../../components';
import { LAYOUT } from '../../../constants/layout';
import { FeedCard } from './card';
import { FontAwesome } from '@expo/vector-icons';
import { FilterModal } from './FilterModal';

export function FeedScreen({}) {
  // theme
  const { colors } = useTheme() as AppTheme;

  // navigation
  const navigation = useNavigation();

  // state
  const [showFilter, setShowFilter] = useState(false);

  return (
    <BlockBackground flex canScroll>
      <BlockBody alignCenter>
        <Header
          right={
            <TouchableOpacity
              row
              alignCenter
              onPress={() => setShowFilter(true)}
            >
              <Text
                fontFamily={'subtitle'}
                color={colors['high-emphasis']}
                margin={[0, 8]}
              >
                Lokasyon seç
              </Text>
              <FontAwesome
                name={'location-arrow'}
                color={colors['high-emphasis']}
              />
            </TouchableOpacity>
          }
        />
        <FeedCard key={'1'} />
        <FeedCard key={'12'} />
        <FeedCard key={'13'} />
        <FeedCard key={'14'} />
        <FeedCard key={'15'} />
      </BlockBody>
      <FilterModal
        open={showFilter}
        onPressClose={() => setShowFilter(false)}
      />
    </BlockBackground>
  );
}
