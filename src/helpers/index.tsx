export function makeHandlerAwareOfAsyncErrors(handler: Function) {
  return async (params: any) => {
    try {
      await handler(params);
    } catch (error) {
      console.log('ERROR :: ', error);
    }
  };
}
