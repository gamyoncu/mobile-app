//
interface AddressProps {
  Id: number;
  AddressTypeId: number;
  CountryName: string;
  StateName?: string;
  CityName?: string;
  Name: string;
  Notes: string;
  ZipCode: string;
  Phone?: string;
  IsActive: boolean;
  IsDefault: boolean;
  CreateDate: string;
  UpdateDate?: string;
}
