import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { FlashMessage } from './src/components/flashMessage/lib';

import useCachedResources from './src/hooks/useCachedResources';
import useColorScheme from './src/hooks/useColorScheme';
import Navigation from './src/navigation';

import { PersistGate } from 'redux-persist/integration/react';
import { Provider } from 'react-redux';
import { store, persistor } from './src/redux/store';
import MockScreen from './Mock';
import { Text } from 'react-native';

export default function App() {
  // return <MockScreen />;

  // return (
  //   <CountryPicker
  //     modalProps={{
  //       visible: true,
  //     }}
  //     onSelect={() => {}}
  //     countryCode={'TR'}
  //   />
  // );

  const isLoadingComplete = useCachedResources();
  // const colorScheme = useColorScheme();

  // return (
  //   <Text
  //     style={{
  //       fontFamily: 'monserrat-bold',
  //       fontSize: 60,
  //       lineHeight: 48,
  //     }}
  //   ></Text>
  // );
  if (!isLoadingComplete) {
    return null;
  } else {
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <SafeAreaProvider>
            <Navigation colorScheme={'light'} />
            <StatusBar />
            <FlashMessage position="top" />
          </SafeAreaProvider>
        </PersistGate>
      </Provider>
    );
  }
}
